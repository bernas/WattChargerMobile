import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, List, ListItem, Left, Body, Button, Thumbnail } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/CarPickerStyle'

export default class CarPicker extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // Defaults for props
  static defaultProps = {
    cars: [],
    callback: (item) => {console.tron.log('PlaceID selected: '+item)}
  }


  render () {
    return (
      <View style={this.props.style}>
         <List
          dataArray={this.props.cars}
          keyboardShouldPersistTaps='always'
          renderRow={(item) =>
            <View>
              <ListItem button avatar onPress={() => this.props.callback(item)} >
                <Left>
                  <Thumbnail square style={{resizeMode: 'contain'}} source={{uri: item.ev.avatar}}/>
                </Left>
                <Body>
                  <Text>{item.ev.brand}</Text>
                  <Text>{item.ev.model}</Text>
                </Body>
              </ListItem>
            </View>
          }
        />
      </View>
    )
  }
}
