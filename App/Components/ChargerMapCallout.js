import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { Callout } from 'react-native-maps'
import Styles from './Styles/ChargerMapCalloutStyles'

import ApiOCM from '../Services/ApiOCM'

export default class ChargerMapCallout extends React.Component {
  constructor (props) {
    super(props)

    const info = ""

    this.state = {
      info
    }

    this.getData(props.location)

    //this.getData(props.location)

    this.onPress = this.props.onPress.bind(this, this.props.location)
  }

  render () {
    /* ***********************************************************
    * Customize the appearance of the callout that opens when the user interacts with a marker.
    * Note: if you don't want your callout surrounded by the default tooltip, pass `tooltip={true}` to `Callout`
    *************************************************************/
    const { location } = this.props
    return (
      <Callout style={Styles.callout}>
        <TouchableOpacity onPress={this.onPress}>
          <Text>{this.state.info}</Text>
        </TouchableOpacity>
      </Callout>
    )
  }


  getData = async (location) => { 
    const api = ApiOCM.create()
    const poi_info = await api.getChargerInfo(location.title).then( (response) => {
        this.state.info = response.data[0].DataProvidersReference
    })
  }



}