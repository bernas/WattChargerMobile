import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { Images } from '../Themes'
import { View, Container, Text, Icon, Button, Header, Body, Left, Right, Card, CardItem, Thumbnail, List, ListItem } from 'native-base'
import { Image } from 'react-native'
import Modal from 'react-native-modal'
import styles from './Styles/ChargerDetailsStyle'
import StarRating from 'react-native-star-rating'
import { SegmentedControls } from 'react-native-radio-buttons'

import ApiOCM from '../Services/ApiOCM'

export default class ChargerDetails extends Component {

  static defaultProps = {
    location : {}
  }

  constructor(props) {
    super(props)
    this.state = {
      data: null,
      ratingModal: false,
      availModal: false,
      detailsModal: false,
      starCount: 0
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.id!=null) {
      this.fetchData(newProps.id)
    }
  }

  fetchData(id) {
    api = ApiOCM.create()
    api.getChargerInfo(id)
    .then((result) => {
      if (result) {
        this.setState({data: result.data[0]})
      }
    })
    console.tron.log(this.state.data);
  }

  handleRatingModal = () => {
    this.setState({ ratingModal: !this.state.ratingModal, availModal: false, detailsModal: false })
  };

  handleSendRating = () => {
    this.setState({ ratingModal: !this.state.ratingModal })
  };

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  handleAvailModal = () => {
    this.setState({ availModal: !this.state.availModal, ratingModal: false, detailsModal: false })
  };

  handleSendAvail = () => {
    this.setState({ availModal: !this.state.availModal })
  };

  handleDetailsModal = () => {
    this.setState({ detailsModal: !this.state.detailsModal, ratingModal: false, availModal: false })
  };

  render () {
    const options = [
      "Yes",
      "No"
    ];
    function setSelectedOption(selectedOption){
      this.setState({
        selectedOption
      });
    }
  
    function renderContainer(optionNodes){
      return <View>{optionNodes}</View>;
    }

  /*let powerKw = null, connType = null
    if (this.state.data != null) {
      if (this.state.data.Connections != null) {
        if (this.state.data.Connections[0].PowerKW) powerKw = (this.state.data.Connections[0].PowerKW).toString() + " Kw";
        //if (this.state.data.Connections[0].ConnectionType) {
          //if (this.state.data.Connections[0].ConnectionType.Title) connType = (this.state.data.Connections[0].ConnectionType.Title).toString();
        //}
        for (i=1; i < this.state.data.Connections.length-1; i++) {
          if (this.state.data.Connections[i].PowerKW) powerKw += ", " + (this.state.data.Connections[i].PowerKW).toString() + " Kw";
          //if (this.state.data.Connections[i].ConnectionType) {
            if (this.state.data.Connections[i].ConnectionType.Title) connType += ", " + (this.state.data.Connections[i].ConnectionType.Title).toString();
          //}
        }
      }
    }*/
    
    return (
      <View>
      <Modal style={styles.chargeDetailWrapper} isVisible={this.props.visible} backdropOpacity={0} onSwipe={this.props.close} onBackdropPress={this.props.close} comments={this.props.comments} swipeDirection='down'>
        <Card style={styles.topCard}>
         <CardItem style={styles.card}>
           <Left>
             <Button 
             onPress={this.props.close}
             transparent
             >
             <Icon name="md-close-circle"/>
             </Button>
             <Body>
               <Button transparent onPress={this.handleDetailsModal}>
               <Text style={{ color: 'black' }}>
                { (this.state.data ==null || this.state.data.AddressInfo.Title==null)? "Test" : this.state.data.AddressInfo.Title}
               </Text>
               </Button>
             </Body>
           </Left>
           <Right>
             <Button transparent>
             <Text>{ (this.state.data==null || this.state.data.OperatorInfo.Title==null)? "Test" : this.state.data.OperatorInfo.Title}</Text>
             </Button>
           </Right>
         </CardItem>
         <CardItem cardBody>
           { (this.state.data==null) ? <Text>"Test"</Text> : this.state.data.Connections.map((val) => {
             <Text>
               {val.Level}
             </Text>
           }) }
         </CardItem>
         <CardItem style={styles.card}>
           <Left>
            <Button transparent onPress={this.handleAvailModal}>
               <Text>Availability: </Text>
               <Text style={{color: 'green'}}>87%</Text>
             </Button>
             <Button transparent style={{ marginLeft: '19%' }} onPress={this.handleRatingModal}>
               <Icon name="md-star-outline" style={{color: 'gold'}}/>
               <Text> 4.3</Text>
             </Button>
           </Left>
           <Right>
             <Button transparent onPress={this.props.comments}>
               <Icon name="chatbubbles" style={{color: 'grey'}}/>
               <Text style={{color: 'grey'}}> 4 comments</Text>
             </Button>
             </Right>
         </CardItem>
        </Card>
        {this.state.ratingModal && <Card style={styles.topCard}>
         <CardItem style={styles.card}>
           <Left>
             <Button 
              onPress={this.handleRatingModal}
              transparent>
             <Icon name="md-close-circle" />
             </Button>
             <Body>
               <Text>Rate this charger:</Text>
             </Body>
           </Left>
           <Right>
           <Button 
              onPress={this.handleSendRating}
              transparent>
             <Icon name="md-send" />
             </Button>
           </Right>
         </CardItem>
         <CardItem>
          <StarRating
            disabled={false}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={5}
            rating={this.state.starCount}
            selectedStar={(rating) => this.onStarRatingPress(rating)}
            starColor={'teal'}
          />
         </CardItem>
        </Card>}
        {this.state.availModal && <Card style={styles.topCard}>
         <CardItem style={styles.card}>
           <Left>
             <Button 
              onPress={this.handleAvailModal}
              transparent>
             <Icon name="md-close-circle" />
             </Button>
             <Body>
               <Text>Available?</Text>
             </Body>
           </Left>
           <Right>
           <Button 
              onPress={this.handleSendAvail}
              transparent>
             <Icon name="md-send" />
             </Button>
           </Right>
         </CardItem>
         <CardItem>
          <Body>
          <SegmentedControls
              options={ options }
              onSelection={ setSelectedOption.bind(this) }
              selectedOption={ this.state.selectedOption }
            />
          </Body>
         </CardItem>
        </Card>}
        {this.state.detailsModal && <Card style={styles.topCard}>
         <CardItem style={styles.card}>
           <Left>
             <Button 
              onPress={this.handleDetailsModal}
              transparent>
             <Icon name="md-close-circle" />
             </Button>
             <Body>
               <Text>Details</Text>
             </Body>
           </Left>
           <Right/>
         </CardItem>
         <CardItem>
         <List style={styles.list}>
            <ListItem>
              <Left>
                <Text>Status</Text>
              </Left>
                <Text note>{this.state.data == null ? "Unknown" : this.state.data.StatusType.Title}</Text>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Town</Text>
              </Left>
                <Text note>{this.state.data == null ? "Unknown" : this.state.data.AddressInfo.Town}</Text>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Adress</Text>
              </Left>
                <Text note>{this.state.data == null ? "Unknown" : this.state.data.AddressInfo.AddressLine1}</Text>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Type</Text>
              </Left>
                <Text note>{this.state.data == null ? "Unknown" : this.state.data.UsageType.Title}</Text>
            </ListItem>
            </List>
         </CardItem>
        </Card>}
        </Modal>
        </View>
    )

  }
}
