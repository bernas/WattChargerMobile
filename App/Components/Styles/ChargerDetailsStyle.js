import { Metrics, Colors } from '../../Themes'
import { Dimensions, StyleSheet } from 'react-native'
var width = Dimensions.get("window").width; //full width
var height = Dimensions.get("window").height; //full width

export default StyleSheet.create({
  chargeDetailWrapper:{
    justifyContent: "flex-end",
    margin: 0,
    marginBottom: 55
  },
  topCard: {
    flex: -1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  card: {
    borderRadius: 20
  },
  list: {
    width: Metrics.screenWidth - 35,
    paddingBottom: Metrics.doubleBaseMargin,
    backgroundColor: 'white'
  }
})
