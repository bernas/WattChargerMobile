import React from 'react'
import { View, Image } from 'react-native'
import MapView, { Polyline} from 'react-native-maps'
import RouteMapCallout from './RouteMapCallout'
import Styles from './Styles/RouteMapStyles'
import { Images } from '../Themes/'
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// import { calculateRegion } from '../Lib/MapHelpers'
var CHEAPEST_JOURNEY = "CHEAPEST_JOURNEY";
var FASTEST_JOURNEY = "FASTEST_JOURNEY";
var MO_FASTEST_JOURNEY = "MO_FASTEST_JOURNEY";

var ROUTE_COLOR = "#aaaaaa";
var BORDER_ROUTE_COLOR = "#888888";

var CHEAPEST_ROUTE_COLOR = '#99CC00';
var FASTEST_ROUTE_COLOR = '#FF4444';
var MO_FASTEST_ROUTE_COLOR = '#1c94c4';

var CHEAPEST_ROUTE_BORDER_COLOR = "#70A300";
var FASTEST_ROUTE_BORDER_COLOR = "#D30808";
var MO_FASTEST_ROUTE_BORDER_COLOR = "#165694";

class RouteMap extends React.Component {

  constructor (props) {
    super(props)
    /* ***********************************************************
    * STEP 1
    * Set the array of locations to be displayed on your map. You'll need to define at least
    * a latitude and longitude as well as any additional information you wish to display.
    *************************************************************/
    /* ***********************************************************
    * STEP 2
    * Set your initial region either by dynamically calculating from a list of locations (as below)
    * or as a fixed point, eg: { latitude: 123, longitude: 123, latitudeDelta: 0.1, longitudeDelta: 0.1}
    * You can generate a handy `calculateRegion` function with
    * `ignite generate map-utilities`
    *************************************************************/
    // const region = calculateRegion(locations, { latPadding: 0.05, longPadding: 0.05 })
    const region = { latitude:50.470474, longitude:10.124153, latitudeDelta: 7, longitudeDelta: 7}
    this.state = {
      region,
      routes : [],
      locations : [],
      from: this.props.from,
      to: this.props.to,
      showUserLocation: false,
      fromCallback: this.props.fromCallback,
      toCallback: this.props.toCallback
    }
    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentWillReceiveProps (newProps) {
    this.setState({from: newProps.from, to: newProps.to, locations: newProps.locations, routes: newProps.routes})
  }

  onRegionChange (newRegion) {
    /* ***********************************************************
    * STEP 4
    * If you wish to fetch new locations when the user changes the
    * currently visible region, do something like this:
    *************************************************************/
    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta / 2,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta / 2,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta / 2,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta / 2
    // }
    // Fetch new data...
  }

  calloutPress (location) {
    /* ***********************************************************
    * STEP 5
    * Configure what will happen (if anything) when the user
    * presses your callout.
    *************************************************************/
  }

  renderMapMarkers (location,index) {
    /* ***********************************************************
    * STEP 6
    * Customize the appearance and location of the map marker.
    * Customize the callout in ./RouteMapCallout.js
    *************************************************************/
    return (
      <MapView.Marker key={index} coordinate={{latitude: location.coordinates.latitude, longitude: location.coordinates.longitude}}>
        <Image source={Images.pinLvl0} style={{resizeMode: 'contain', width: 40, height:40}}/>
        <RouteMapCallout location={location} onPress={this.calloutPress} />
      </MapView.Marker>
    )
  }

  renderPolylines(route,index) {
    switch(route.type) {
      case CHEAPEST_JOURNEY:
          color = CHEAPEST_ROUTE_COLOR
          break;
      case FASTEST_JOURNEY:
          color = FASTEST_ROUTE_COLOR
          break;
      case MO_FASTEST_JOURNEY:
          color = MO_FASTEST_ROUTE_COLOR
          break;
      default:
          color = "#000"
    }
    return (
        <Polyline
          key={index}
          coordinates={route.route}
		      strokeColor={color} // fallback for when `strokeColors` is not supported by the map-provider
		      strokeWidth={4}
	    />
    )
  }

  renderFrom(pin) {
    return (
      <MapView.Marker key={pin.name} 
                      coordinate={{ latitude: pin.latitude, longitude: pin.longitude }} 
                      onDragEnd={(e) => this.state.fromCallback(e)} 
                      draggable>
        {/* <Image style={{width: 30, height: 30, resizeMode: 'center'}} source={Images.startstop}/> */}
      </MapView.Marker>
    )
  }

  renderTo(pin) {
    return (
      <MapView.Marker key={pin.name}
        coordinate={{ latitude: pin.latitude, longitude: pin.longitude }}
        onDragEnd={(e) => this.state.toCallback(e)}
        draggable>
        {/* <Image style={{width: 30, height: 30, resizeMode: 'center'}} source={Images.startstop}/> */}
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        initialRegion={this.state.region}
        onRegionChangeComplete={this.onRegionChange}
        showsUserLocation={this.state.showUserLocation}
      >
        {this.state.locations.map((location,index) => this.renderMapMarkers(location,index))}
        {(this.state.from!=null) && this.renderFrom(this.state.from)}
        {(this.state.to!=null) && this.renderTo(this.state.to)}
        {this.state.routes.map((route,index) => this.renderPolylines(route,index))}
      </MapView>
    )
  }
}

export default RouteMap
