import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, List, ListItem, Left, Body, Button } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/SearchResultsStyle'

export default class SearchResults extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // Defaults for props
  static defaultProps = {
    predictions: {
      inText : "",
      pred : {},
    },
    callback: (item) => {console.tron.log('PlaceID selected: '+item)}
  }


  render () {
    return (
      <View style={this.props.style}>
        { (this.props.predictions.inText!="") && <List
          dataArray={this.props.predictions.pred}
          keyboardShouldPersistTaps='always'
          renderRow={(item) =>
            <View>
              <ListItem button avatar onPress={() => this.props.callback(item.placeID)} >
                <Left>
                  <Icon name="location-on"/>
                </Left>
                <Body>
                  <Text>{item.primaryText}</Text>
                  <Text>{item.secondaryText}</Text>
                </Body>
              </ListItem>
            </View>
          }
        />}
      </View>
    )
  }
}
