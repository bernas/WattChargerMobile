import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Images, Colors } from '../Themes'
import MapView from 'react-native-maps'

const offset_map_small = 0.0001;

export default class Marker extends React.Component {

  state = {
    colorByCategory: {
      A: "violet",
      B: "yellow",
      C: "blue",
      D: "pink",
      E: "green",
      "Cluster": "red"
    }
  }

  onPress() {
    if (!this.props.feature.properties.featureclass) {
      //  Calculer l'angle
      const { region } = this.props;
      const category = this.props.feature.properties.featureclass || "Cluster";
      const angle = region.longitudeDelta || 0.0421/1.2;
      const result =  Math.round(Math.log(360 / angle) / Math.LN2);
      //  Chercher les enfants
      const markers = this.props.clusters["places"].getChildren(this.props.feature.properties.cluster_id, result);
      const newRegion = [];
      const smallZoom = 0.05;
      //  Remap
      markers.map(function (element) {
        newRegion.push({
          latitude: offset_map_small + element.geometry.coordinates[1] - region.latitudeDelta * smallZoom,
          longitude: offset_map_small + element.geometry.coordinates[0] - region.longitudeDelta * smallZoom,
        });

        newRegion.push({
          latitude: offset_map_small + element.geometry.coordinates[1],
          longitude: offset_map_small + element.geometry.coordinates[0],
        });

        newRegion.push({
          latitude: offset_map_small + element.geometry.coordinates[1] + region.latitudeDelta * smallZoom,
          longitude: offset_map_small + element.geometry.coordinates[0] + region.longitudeDelta * smallZoom,
        });
      });
      //  Préparer the retour
      const options = {
        isCluster: true,
        region: newRegion,
      };
      //  Ensuite envoyer l'événement
      if (this.props.onPress) {
        this.props.onPress({
          type: category,
          feature: this.props.feature,
          options: options,
        });
      }
    }
    else {
      this.props.onPress({options: { isCluster: false},_id: this.props.feature.properties._id})
    }
  }

  renderMapMarker () {
    /* ***********************************************************
    * STEP 6
    * Customize the appearance and location of the map marker.
    * Customize the callout in ./ChargerMapCallout.js
    *************************************************************/
    const location = {
      title: this.props.feature.properties._id,
      latitude : this.props.feature.geometry.coordinates[1],
      longitude : this.props.feature.geometry.coordinates[0],
    }
    switch(this.props.feature.properties.chargeSpeed) {
      case 1:
      return(<MapView.Marker
      key={location.title}
      coordinate= {{ latitude: location.latitude, longitude: location.longitude }}
      onPress= {this.onPress.bind(this)}
      >
      <Image source={Images.pinLvl0} style={{resizeMode: 'contain', width: 40, height:40}}/>
      </MapView.Marker>)
      break;
      case 2:
      return(<MapView.Marker
      key={location.title}
      coordinate= {{ latitude: location.latitude, longitude: location.longitude }}
      onPress= {this.onPress.bind(this)}
      >
      <Image source={Images.pinLvl1} style={{resizeMode: 'contain', width: 40, height:40}}/>
      </MapView.Marker>)
      break;
      case 3:
      return(<MapView.Marker
      key={location.title}
      coordinate= {{ latitude: location.latitude, longitude: location.longitude }}
      onPress= {this.onPress.bind(this)}
      >
      <Image source={Images.pinLvl2} style={{resizeMode: 'contain', width: 40, height:40}}/>
      </MapView.Marker>)
      break;
      default:
      return(<MapView.Marker
      key={location.title}
      coordinate= {{ latitude: location.latitude, longitude: location.longitude }}
      onPress= {this.onPress.bind(this)}
      >
      <Image source={Images.pinPrivate} style={{resizeMode: 'contain', width: 40, height:40}}/>
      </MapView.Marker>)
    }
  }

  render() {
    const latitude = this.props.feature.geometry.coordinates[1];
    const longitude = this.props.feature.geometry.coordinates[0];
    const category = this.props.feature.properties.featureclass || "Cluster";
    const text = (category  == "Cluster" ? this.props.feature.properties.point_count : category);
    const size = 37;
    return (
      (category == "Cluster") ? 
      <MapView.Marker 
      coordinate={{latitude,longitude,}} 
      onPress={this.onPress.bind(this)}
      >
        <Text style={{ backgroundColor: Colors.ricePaper, borderWidth: 1, borderColor: '#000', borderRadius: 10, padding: 5, overflow: 'hidden' }}>
        {text}
        </Text>
      </MapView.Marker> 
      : 
      (this.renderMapMarker())
    );
  }
}