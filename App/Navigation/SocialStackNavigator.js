import { StackNavigator } from 'react-navigation'

import SocialScreen from '../Containers/SocialScreen'
import PostScreen from '../Containers/PostScreen'

import styles from './Styles/NavigationStyles'


export default StackNavigator({
    SocialScreen: {
        screen: SocialScreen
    },
    PostScreen: {
        screen: PostScreen
    }
},
    {
        headerMode: 'none',
        navigationOptions: {
            headerStyle: styles.header,
        }
})