import { StackNavigator } from 'react-navigation'
import WelcomeScreen from '../Containers/WelcomeScreen'
import RegisterScreen from '../Containers/RegisterScreen'
import LoginScreen from '../Containers/LoginScreen'

import styles from './Styles/NavigationStyles'

export default StackNavigator({
    WelcomeScreen: {
        screen: WelcomeScreen,
        navigationOptions: { title: 'Welcome' }
    },
    LoginScreen: {
        screen: LoginScreen
    },
    RegisterScreen: {
        screen: RegisterScreen
    }
}, {
    headerMode: 'none',
    initialRouteName: 'WelcomeScreen',
    navigationOptions: {
        headerStyle: styles.header,
        gesturesEnabled: false,
    }
})