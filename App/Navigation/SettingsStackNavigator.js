import { StackNavigator } from 'react-navigation'

import SettingsScreen from '../Containers/SettingsScreen'
import MyEVsSettingsScreen from '../Containers/MyEVsSettingsScreen'
import PrivacySettingsScreen from '../Containers/PrivacySettingsScreen'
import MapNavSettingsScreen from '../Containers/MapNavSettingsScreen'
import ProfileSettingsScreen from '../Containers/ProfileSettingsScreen'
import AccountSettingsScreen from '../Containers/AccountSettingsScreen'
import AddEvScreen from '../Containers/AddEvScreen'
import SpecificationsScreen from '../Containers/SpecificationsScreen'

import styles from './Styles/NavigationStyles'


export default StackNavigator({
    SettingsScreen: {
        screen: SettingsScreen
    },
    MyEVsSettingsScreen: {
        screen: MyEVsSettingsScreen
    },
    PrivacySettingsScreen: {
        screen: PrivacySettingsScreen
    },
    ProfileSettingsScreen: {
        screen: ProfileSettingsScreen
    },
    MapNavSettingsScreen: {
        screen: MapNavSettingsScreen
    },
    AccountSettingsScreen: {
        screen: AccountSettingsScreen
    },
    AddEvScreen: {
      screen: AddEvScreen
    },
    SpecificationsScreen: {
      screen: SpecificationsScreen
    }
},
    {
        headerMode: 'none',
        navigationOptions: {
            headerStyle: styles.header,
        }
})
