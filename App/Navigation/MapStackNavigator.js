import { StackNavigator } from 'react-navigation'

import RoutePlannerScreen from '../Containers/RoutePlannerScreen'
import MapScreen from '../Containers/MapScreen'
import ChargerCommentsScreen from '../Containers/ChargerCommentsScreen'

import styles from './Styles/NavigationStyles'


export default StackNavigator({
    MapScreen: {
        screen: MapScreen
    },
    RoutePlannerScreen: {
        screen: RoutePlannerScreen
    },
    ChargerCommentsScreen: {
        screen: ChargerCommentsScreen
    },
},
    {
        headerMode: 'none',
        navigationOptions: {
            headerStyle: styles.header,
        }
})