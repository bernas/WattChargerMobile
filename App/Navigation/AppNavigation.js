import React, {PropTypes} from 'react'
import { connect } from 'react-redux'
import { StackNavigator, addNavigationHelpers } from 'react-navigation'

import LoadingScreen from '../Containers/LoadingScreen'
import LoggedInTabNavigator from './LoggedInTabNavigator'
import NotLoggedInStackNavigator from './NotLoggedInStackNavigator'

import styles from './Styles/NavigationStyles'


export const PrimaryNav = StackNavigator(
	{
		LoadingScreen: { screen: LoadingScreen },
		LoggedInStack: { screen: LoggedInTabNavigator },
		NotLoggedInStack: { screen: NotLoggedInStackNavigator }
	},
	{
		headerMode: "none",
		navigationOptions: {
			headerStyle: styles.header,
		}
	}
)

const Navigation = ({ dispatch, navigation }) => {
	return (
		<PrimaryNav
		  navigation={addNavigationHelpers({dispatch, state: navigation})}
		/>
	)
}

Navigation.propTypes = {
	dispatch: PropTypes.func.isRequired,
	navigation: PropTypes.object.isRequired
}

function mapStateToProps (state) {
	return {
		navigation: state.navigation
	}
}

export default connect(mapStateToProps)(Navigation);
