import React from 'react'
import { TabNavigator } from 'react-navigation'
import { NavigationComponent } from 'react-native-material-bottom-navigation'
import { Icon } from 'native-base'
import { Colors } from '../Themes'

import SettingsStackNavigator from './SettingsStackNavigator'
import MapScreen from '../Containers/MapScreen'
import SettingsScreen from '../Containers/SettingsScreen'
import SocialStackNavigator from './SocialStackNavigator'
import MyEVsSettingsScreen from '../Containers/MyEVsSettingsScreen'
import PrivacySettingsScreen from '../Containers/PrivacySettingsScreen'
import MapNavSettingsScreen from '../Containers/MapNavSettingsScreen'
import MapStackNavigator from './MapStackNavigator'

import styles from './Styles/NavigationStyles'

export default TabNavigator({
    Map: {
        screen: MapStackNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (<Icon name="ios-navigate" size={25} color={tintColor} style={{color:Colors.ricePaper}}/>)
        }
    },
    Social: {
        screen: SocialStackNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (<Icon name="md-person" size={25} color={tintColor} style={{color:Colors.ricePaper}}/>)
        }
    },
    Settings: {
        screen: SettingsStackNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (<Icon name="md-settings" size={25} color={tintColor} style={{color:Colors.ricePaper}}/>)
        }
    },
},
    {
        headerMode: 'none',
        tabBarComponent: NavigationComponent,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            headerStyle: styles.header,
            animationEnabled: true,
            bottomNavigationOptions: {
                labelColor: Colors.ricePaper,
                rippleColor: 'white',
                backgroundColor: Colors.background,
                tabs: {
                  Map: {
                    activeLabelColor: 'white',
                    activeIcon: <Icon size={35} style={{color:'white'}} name="ios-navigate" />

                  },
                  Social: {
                    activeLabelColor: 'white',
                    activeIcon: <Icon size={35} style={{color:'white'}} name="md-person" />

                  },
                  Settings: {
                    activeLabelColor: 'white',
                    activeIcon: <Icon size={35} style={{color:'white'}} name="md-settings" />
                  }
                }
            }
        }
})