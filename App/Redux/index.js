import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    navigation: require('./NavigationRedux').reducer,
    appState: require('./AppStateRedux').reducer,
    github: require('./GithubRedux').reducer,
    login: require('./LoginRedux').reducer,
    transientlogin: require('./TransientLoginRedux').reducer,
    register: require('./RegisterRedux').reducer,
    search: require('./SearchRedux').reducer,
    map: require('./MapRedux').reducer,
    chargermap: require('./ChargerMapRedux').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
