import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  transientLoginRequest: ['token'],
  transientLoginSuccess: ['token'],
  transientLoginFailure: ['error'],
  logout: null
})

export const TransientLoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  token: null,
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */


// we're attempting to login
export const request = (state) => state.merge({ fetching: true })

// we've successfully logged in
export const success = (state, { token }) =>
  state.merge({ fetching: false, error: null, token })

// we've had a problem logging in
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

// we've logged out
export const logout = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TRANSIENT_LOGIN_REQUEST]: request,
  [Types.TRANSIENT_LOGIN_SUCCESS]: success,
  [Types.TRANSIENT_LOGIN_FAILURE]: failure
})
