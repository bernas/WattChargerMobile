import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  searchRequest: ['inputText'],
  searchResponse: ['predictions'],
  searchFailure: null
})

export const MapTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  inputText: null,
  fetching: null,
  predictions: null,
  error: null
})

/* ------------- Reducers ------------- */

// we want to search
export const request = (state, { inputText }) =>
  state.merge({ fetching: true, inputText, predictions: null})
 
// we've successfully logged in
export const success = (state, { predictions }) =>
  state.merge({ fetching: false, error: null, predictions })

// we've had a problem logging in
export const failure = state =>
  state.merge({ fetching: false, error: true, predictions: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEARCH_REQUEST]: request,
  [Types.SEARCH_RESPONSE]: success,
  [Types.SEARCH_FAILURE]: failure,
})

/* ------------- Selectors ------------- */
