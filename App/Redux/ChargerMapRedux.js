import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  chargerMapRequest: ['data'],
  chargerMapSuccess: ['locations'],
  chargerMapFailure: null
})

export const ChargerMapTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  locations: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, locations: null })

// successful api lookup
export const success = (state, action) => {
  const { locations } = action
  return state.merge({ fetching: false, error: null, locations })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, locations: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHARGER_MAP_REQUEST]: request,
  [Types.CHARGER_MAP_SUCCESS]: success,
  [Types.CHARGER_MAP_FAILURE]: failure
})
