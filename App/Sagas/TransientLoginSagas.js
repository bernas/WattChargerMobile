import { put } from 'redux-saga/effects'
import TransientLoginActions from '../Redux/TransientLoginRedux'

// attempts to login
export function * transientLogin ({ token }) {
  if (token == null) {
    // dispatch failure
    yield put(TransientLoginActions.transientLoginFailure('WRONG'))
  } else {
    // dispatch successful logins
    yield put(TransientLoginActions.transientLoginSuccess(token))
  }
}

