import { put } from 'redux-saga/effects'
import RegisterActions from '../Redux/RegisterRedux'

// attempts to login
export function * register ({ token }) {
  if (token == null) {
    // dispatch failure
    yield put(RegisterActions.registerFailure('WRONG'))
  } else {
    // dispatch successful logins
    yield put(RegisterActions.registerSuccess(token))
  }
}
