import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { TransientLoginTypes } from '../Redux/TransientLoginRedux'
import { RegisterTypes } from '../Redux/RegisterRedux'
import { MapTypes }   from '../Redux/MapRedux'
import { ChargerMapTypes } from '../Redux/ChargerMapRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { login } from './LoginSagas'
import { transientLogin } from './TransientLoginSagas'
import { register } from './RegisterSagas'
import { getUserAvatar } from './GithubSagas'
import { search } from './MapSagas'
import { getChargerMap } from './ChargerMapSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(LoginTypes.LOGIN_REQUEST, login),
    takeLatest(RegisterTypes.REGISTER_REQUEST, register ),
    takeLatest(MapTypes.SEARCH_REQUEST, search),
    takeLatest(ChargerMapTypes.CHARGER_MAP_REQUEST, getChargerMap),
    takeLatest(TransientLoginTypes.TRANSIENT_LOGIN_REQUEST, transientLogin),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api)
  ])
}
