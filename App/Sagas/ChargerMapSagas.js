/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import ChargerMapActions from '../Redux/ChargerMapRedux'
import ApiOCM from '../Services/ApiOCM'

export function * getChargerMap (api, action) {
  // make the call to the api
  const appi = yield call(ApiOCM.create)
  const response = yield call(appi.getGermanyChargers)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(ChargerMapActions.chargerMapSuccess(response.data))
  } else {
    yield put(ChargerMapActions.chargerMapFailure())
  }
}

/*yield takeLatest('RECORDS/FETCH', function* async fetchRecords() {

  try {
      const response = await fetch('https://api.service.com/endpoint');
      const responseBody = response.json();
  } catch (e) {
      yield put(fetchFailed(e));
      return;
  }

  yield put(setRecords(responseBody.records));
});
}*/