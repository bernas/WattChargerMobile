import { put,call } from 'redux-saga/effects'
import MapActions from '../Redux/MapRedux'
import RNGooglePlaces from "react-native-google-places";

// attempts to login
export function * search ({ inputText }) {
  const result = yield call(RNGooglePlaces.getAutocompletePredictions,inputText)
  yield put(MapActions.searchResponse(result))
}
