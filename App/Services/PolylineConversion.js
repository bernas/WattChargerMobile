var allChartOptions = {
  max:110,
  min:0,
  usedSteps: [],
  travelTimes: [],
  data: [] //0-cheapest, 1-fastest
};

export default function iterateAllPlans(_plans) {
    try {
      plans = JSON.parse(_plans).plans
    }
    catch(r) {
        return(null)
    }
    final_result = {routes: [], markers: []}
  allChartOptions.data = [];
  allChartOptions.usedSteps = [];
  for (var i = 0; i < plans.length; i++) {
      var oneBasicRouteLatLngs = [];
      var segments = plans[i].segments;
      var type = plans[i].type;
      var statesOfCharge = [];
      var lengths = [];
      var travelTimes = [];
      var XYData = [];
      var usedSteps = [];
      var chargers = [];
      var travelTime = 0;
      var length = 0;
      var numOfSOC = 0;

      for (var j = 0; j < (segments.length); j++) {
          var from = segments[j].from;
          if (from.type == "SUPERCHARGER") {
              from.coordinates= createLatLng(from.coordinates)
              chargers.push(from);
          }

          var steps = [];
          steps = steps.concat(from, segments[j].viaSteps);

          var to = segments[j].to;
          if (to.type == "DESTINATION") {
              steps.push(to);
          }

          for (var k = 1; k < (steps.length-1); k++) {
              var latLon = createLatLng(steps[k].coordinates);
              oneBasicRouteLatLngs.push(latLon);
              var stateOfCharge = steps[k].leavingStateOfCharge;
              var maxSoc = 85000;
              if (stateOfCharge > 0) {
                  //console.log(length+","+stateOfCharge + ", " + L.latLng(lat, lng));
                  stateOfCharge = (stateOfCharge/maxSoc)*100;
                  statesOfCharge.push(stateOfCharge);
                  //console.log(stateOfCharge + ", " + length);
                  lengths.push(length);
                  travelTimes.push(travelTime);
                  XYData.push([length, stateOfCharge]);
                  usedSteps.push(latLon);
                  numOfSOC++;
              }

              var ttToNext = steps[k].travelTimeToNextStep;
              var distToNext = steps[k].distanceToNextStep;
              if (distToNext > 0) {
                  length += distToNext;
                  travelTime += ttToNext;
              }
          }
      }

      final_result.markers.push(chargers)
      final_result.routes.push({route: oneBasicRouteLatLngs,type: type})

      //createButtonForRoute(plans[i], i);
      allChartOptions.data.push(XYData);
      allChartOptions.usedSteps.push(usedSteps);
      allChartOptions.travelTimes.push(travelTimes);
  }
  return(final_result)
}

function createLatLng(coordinates) {
    var lat = coordinates.latE6 / 1E6;
    var lng = coordinates.lonE6 / 1E6;
    return {latitude: lat,longitude: lng};
}
