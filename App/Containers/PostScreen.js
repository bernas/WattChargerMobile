import React, { Component } from 'react'
import { BackHandler, Image } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, Card, CardItem, Thumbnail, Input } from 'native-base'
import { connect } from 'react-redux'
import { Images} from "../Themes";
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import Modal from "react-native-modal";
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PostScreenStyle'

class PostScreen extends Component {

  constructor(props){
    super(props);

    this.state = { 
          post: null,
          showModal: false,
          inputValue: '',
          commentsList: null
    };
  }

  timeDifference(current, previous) {

    const milliSecondsPerMinute = 60 * 1000
    const milliSecondsPerHour = milliSecondsPerMinute * 60
    const milliSecondsPerDay = milliSecondsPerHour * 24
    const milliSecondsPerMonth = milliSecondsPerDay * 30
    const milliSecondsPerYear = milliSecondsPerDay * 365
  
    const elapsed = current - previous
  
    if (elapsed < milliSecondsPerMinute / 3) {
      return 'just now'
    }
  
    if (elapsed < milliSecondsPerMinute) {
      return 'less than 1 min ago'
    }
  
    else if (elapsed < milliSecondsPerHour) {
      return `${Math.round(elapsed/milliSecondsPerMinute)  } min ago`
    }
  
    else if (elapsed < milliSecondsPerDay ) {
      return `${Math.round(elapsed/milliSecondsPerHour )  }h ago`
    }
  
    else if (elapsed < milliSecondsPerMonth) {
      return `${Math.round(elapsed/milliSecondsPerDay)  } days ago`
    }
  
    else if (elapsed < milliSecondsPerYear) {
      return `${Math.round(elapsed/milliSecondsPerMonth)  } mo ago`
    }
  
    return `${Math.round(elapsed/milliSecondsPerYear )  } years ago`
  }

  timeDifferenceForDate(date) {
    const now = new Date().getTime()
    const updated = new Date(date).getTime()
    return this.timeDifference(now, updated)
  }

  renderComments(item, index) {
    return (
      <Card key={item.insertedAt}>
        <CardItem>
          <Left style={{ width: "160%" }}>
            <Thumbnail source={{ uri: `${item.user.avatar}` }}  style={{ width:40, height:40 }}/>
            <Body>
              <Text>{item.user.username}</Text>
            </Body>
          </Left>
          <Right>
            <Text note>{this.timeDifferenceForDate(item.insertedAt)}</Text>
          </Right>
        </CardItem>
        <CardItem>
          <Body>
            <Text>
              {item.comment}
            </Text>
          </Body>
        </CardItem>
        <CardItem>
          <Left>
            <Button transparent>
              <Icon style={{color: 'grey'}} name="md-arrow-round-up" />
              <Text style={{color: 'grey'}}> {item.upvotes}</Text>
            </Button>
            <Button transparent>
              <Icon style={{color: 'grey'}} name="md-arrow-round-down" />
              <Text style={{color: 'grey'}}> {item.downvotes}</Text>
            </Button>
          </Left>
        </CardItem>
      </Card>
    )
  };

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
    this.setState({
      post: this.props.navigation.state.params.post
    });
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  handleActiveModal = () => {
    this.setState({ showModal: true })
  };

  handleHideModal = () => {
    this.setState({ showModal: false })
  }

  handleSendPostComment = () => {
    if (this.state.inputValue.length > 2) {
      const comment = this.state.inputValue, upvotes = 0, downvotes = 0, postId = parseInt(this.state.post.id);
      let commentPost;
      this.props.CreatePostCommentMutation({
        variables: {
          comment, upvotes, downvotes, postId
        }
      }).then(res => this.updateCommentList(res))
        .catch( err => console.tron.log("ERRO: " + err))
      this.setState({ showModal: false, inputValue: null })
    }
  }

  updateInputValue = (text) => {
    this.setState({
      inputValue: text
    });
  }

  updateCommentList = (res) => {
    const comment = this.renderComments(res.data.createComment, 0);
    this.setState({
      commentsList: comment
    })
  }

  render () {
    let commentsList = null;
    let numberComments = 0;

    if (this.state.post) {
      if (this.state.post.comments) {
        commentsList = this.state.post.comments.map((result, index) => {
          return this.renderComments(result, index);
        });
      }
      numberComments = commentsList.length;
    } else {
      return (
        <Container>
          <Header>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon name="md-arrow-back" style={ styles.title }/>
            </Button>
          </Left>
            <Body>
              <Title>Post</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
            <Text>An error has occurred</Text>
          </Content>
        </Container>
      );
    }

    return (
      <Container>
        <Header style={ styles.header }>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon name="md-arrow-back" style={ styles.title }/>
            </Button>
          </Left>
          <Body>
            <Title style={ styles.title }>Post</Title>
          </Body>
          <Right>
          <Button transparent onPress={this.handleActiveModal}>
              <Icon name="md-add" style={ styles.title }/>
            </Button>
          </Right>

        </Header>

        <Modal 
          style={styles.postCommentWrapper} 
          isVisible={this.state.showModal} 
          backdropOpacity={0} onSwipe={this.handleHideModal} 
          onBackdropPress={this.handleHideModal} 
          swipeDirection='down'>
        <Card style={styles.topCard}>
         <CardItem style={styles.card}>
           <Left>
             <Button 
              onPress={this.handleHideModal}
              transparent>
             <Icon name="ios-close-circle" />
             </Button>
             <Body>
               <Text> Comment this post: </Text>
             </Body>
           </Left>
           <Right>
           <Button 
              onPress={this.handleSendPostComment}
              transparent>
             <Icon name="md-send" />
             </Button>
           </Right>
         </CardItem>
         <CardItem>
           <Input placeholder="Write here.." value={this.state.inputValue} onChangeText={(text) => this.updateInputValue(text)}/>
         </CardItem>
        </Card>
        </Modal>

        <Content padder style={ styles.container} style={{ marginBottom: "0%" }}>
          <Card padder>
            <CardItem>
              <Left style={{ width: "160%" }}>
                <Thumbnail source={{ uri: `${this.state.post.user.avatar}` }} style={{ width:40, height:40 }}/>
                <Body>
                  <Text>{this.state.post.user.username}</Text>
                  <Text note style={{ fontSize: 12 }}></Text>
                </Body>
              </Left>
              <Right>
                <Text note>{this.timeDifferenceForDate(this.state.post.insertedAt)}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Text style={{ fontWeight: "bold" }}>{this.state.post.title}</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{ fontSize: 14 }}>
                  {this.state.post.body}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-up" />
                  <Text style={{color: 'grey'}}> {this.state.post.upvotes}</Text>
                </Button>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-down" />
                  <Text style={{color: 'grey'}}> {this.state.post.downvotes}</Text>
                </Button>
              </Left>
              <Right>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="chatbubbles" />
                  <Text style={{color: 'grey'}}> {numberComments} comments</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          {commentsList}  
          {this.state.commentsList}
        </Content>
      </Container>
    )
  }
}

const CreateCommentPostMutation = gql`
mutation CreatePostCommentMutation($comment: String!, $upvotes: Int!, $downvotes: Int!, $postId: Int!) {
  createComment(comment: $comment, upvotes: $upvotes, downvotes: $downvotes, postId: $postId) {
      id,
      upvotes,
      downvotes,
      comment,
    user{
        username,
        avatar
      },
    insertedAt,
    post{
        id
      }
  }
}
`

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default compose(
  graphql(CreateCommentPostMutation, { name: 'CreatePostCommentMutation' }),
  connect(mapStateToProps, mapDispatchToProps))(PostScreen)

