import React, { Component } from 'react'
import { Images, Metrics } from "../Themes";
import { BackHandler } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Form, Item, Label, Input, Thumbnail, Spinner } from 'native-base'
import { connect } from 'react-redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ProfileSettingsScreenStyle'

class ProfileSettingsScreen extends Component {

  constructor(props) {
		super(props);

		this.state = {
      isNameDisabled: true,
			name: "",
      placeholderName: "",

      isPasswordDisabled: true,
			password: "",
		};
	}

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  componentWillReceiveProps (newProps) {
    oldPlaceholderName = this.state.placeholderName

    this.setState({ placeholderName: newProps.getUserDetails.getCurrentUser.name })

    if (oldPlaceholderName != newProps.getUserDetails.getCurrentUser.name)
      this.setState({ name: "" })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  handlePressNameCreate = () => {
    this.setState({ isNameDisabled: !this.state.isNameDisabled });
  };

  handlePressNameCheckmark = () => {
    this.setState({ isNameDisabled: !this.state.isNameDisabled });
    this.changeNameMutation()
  };

  handlePressNameClose = () => {
    this.setState({ isNameDisabled: !this.state.isNameDisabled });
    this.setState({ name: "" });
  };

	handleChangeName = text => {
		this.setState({ name: text });
	};

  changeNameMutation = () => {
    const name = this.state.name

    this.props.updateUserMutation({
      variables: {
        name
      }
    }).then(res => {
      this.props.getUserDetails.refetch()
    })
  }

  handlePressPasswordCreate = () => {
    this.setState({ isPasswordDisabled: !this.state.isPasswordDisabled });
  };

  handlePressPasswordCheckmark = () => {
    this.setState({ isPasswordDisabled: !this.state.isPasswordDisabled });
    this.changePasswordMutation();
    this.setState({ password: "" });
  };

  handlePressPasswordClose = () => {
    this.setState({ isPasswordDisabled: !this.state.isPasswordDisabled });
    this.setState({ password: "" });
  };

  handleChangePassword = text => {
    this.setState({ password: text });
  };

  changePasswordMutation = () => {
    const password = this.state.password

    this.props.updateUserMutation({
      variables: {
        password
      }
    }).then(res => {
      this.props.getUserDetails.refetch()
    })
  }

  loadingRender () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <Spinner color='#016976' />
        </Content>
      </Container>
    )
  }

  errorRender () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <Text>Ops! An error has occurred!</Text>
        </Content>
      </Container>
    )
  }

  defaultRender () {

    return (
      <Container>

        <Header style={styles.header}>

          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title style={styles.title}>Profile</Title>
          </Body>

          <Right />

        </Header>

        <Content padder style={styles.container}>

          <Thumbnail large source={{uri: this.props.getUserDetails.getCurrentUser.avatar}} style={styles.thumbnail} />

          <Form>

            <Item regular style={styles.item}>
              <Icon active name='md-person' style={styles.iconItem} />
              <Label style={styles.label}>Name</Label>
              <Input
                placeholder={this.state.placeholderName}
                disabled={this.state.isNameDisabled}
                onChangeText={this.handleChangeName}
                value={this.state.name}
                keyboardType="default"
                autoCapitalize="none"
								autoCorrect={false}
              />
              <Button transparent>
                {this.state.isNameDisabled && <Icon active name='md-create' style={styles.iconItem} onPress={this.handlePressNameCreate} />}
                {!this.state.isNameDisabled && <Icon active name='md-checkmark' style={styles.iconItem} onPress={this.handlePressNameCheckmark} />}
                {!this.state.isNameDisabled && <Icon active name='md-close' style={styles.iconItem} onPress={this.handlePressNameClose} />}
              </Button>
            </Item>

            <Item regular style={styles.item}>
              <Icon active name='md-person' style={styles.iconItem} />
              <Label style={styles.label}>Username</Label>
              <Input
                placeholder={this.props.getUserDetails.getCurrentUser.username}
                disabled={true} />
            </Item>

            <Item regular style={styles.item}>
              <Icon active name='md-mail' style={styles.iconItem} />
              <Label style={styles.label}>E-mail</Label>
              <Input
                placeholder={this.props.getUserDetails.getCurrentUser.email}
                disabled={true} />
            </Item>

            <Item regular style={styles.item}>
              <Icon active name='md-lock' style={styles.iconItem} />
              <Label style={styles.label}>Password</Label>
              <Input
                placeholder={"******"}
                disabled={this.state.isPasswordDisabled}
                onChangeText={this.handleChangePassword}
                value={this.state.password}
                keyboardType="default"
                autoCapitalize="none"
								autoCorrect={false}
                secureTextEntry
              />
              <Button transparent>
                {this.state.isPasswordDisabled && <Icon active name='md-create' style={styles.iconItem} onPress={this.handlePressPasswordCreate} />}
                {!this.state.isPasswordDisabled && <Icon active name='md-checkmark' style={styles.iconItem} onPress={this.handlePressPasswordCheckmark} />}
                {!this.state.isPasswordDisabled && <Icon active name='md-close' style={styles.iconItem} onPress={this.handlePressPasswordClose} />}
              </Button>
            </Item>

          </Form>

        </Content>

      </Container>
    )
  }

  render () {
    if (this.props.getUserDetails && this.props.getUserDetails.loading)
      return this.loadingRender();

    if (this.props.getUserDetails && this.props.getUserDetails.error)
      return this.errorRender();

    return this.defaultRender();
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const USER_DETAILS_QUERY = gql`
  query UserDetailsQuery {
    getCurrentUser {
      id,
      name,
      username,
      avatar,
      email
    }
  }
`

const UPDATE_USER_MUTATION = gql`
  mutation UpdateUserMutation($name: String, $password: String) {
    updateUser (
      user: {
        name: $name,
        password: $password
      }
    ) {
      id
    }
  }
`

export default compose(
  graphql(USER_DETAILS_QUERY, { name: 'getUserDetails' }),
  graphql(UPDATE_USER_MUTATION, { name: 'updateUserMutation' }),
  connect(mapStateToProps, mapDispatchToProps)
)(ProfileSettingsScreen)
