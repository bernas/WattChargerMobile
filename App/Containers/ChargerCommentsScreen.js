import React, { Component } from 'react'
import { BackHandler } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, Card, CardItem, Thumbnail } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ChargerCommentsScreenStyle'

class ChargerCommentsScreen extends Component {
  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  render () {
    return (
      <Container>
        <Header style={ styles.header }> 
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon name="md-arrow-back" style={ styles.title }/>
            </Button>
          </Left>
          <Body>
            <Title style={ styles.title }>Charger</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={ styles.container }>
        <Card padder style={{ backgroundColor: "grey" }} key='1'>
            <CardItem>
              <Left style={{ width: "160%" }}>
                <Thumbnail source={{ uri: `https://www.spotteron.net/images/icons/user60.png` }}  style={{ width: 40, height: 40 }} />
                <Body>
                  <Text>ZeCarlos</Text>
                  <Text note style={{ fontSize: 12 }}>{`since 2017`}</Text>
                </Body>
              </Left>
              <Right>
                <Text note>1h ago</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Text style={{ fontWeight: "bold" }}>Comment about this</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{ fontSize: 14 }}>
                  New comment about this charger
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-up" />
                  <Text style={{color: 'grey'}}> 47</Text>
                </Button>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-down" />
                  <Text style={{color: 'grey'}}> 13</Text>
                </Button>
              </Left>
              <Right>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="chatbubbles" />
                  <Text style={{color: 'grey'}}> 21 comments</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChargerCommentsScreen)