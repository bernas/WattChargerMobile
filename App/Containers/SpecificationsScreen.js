import React, { Component } from 'react'
import { BackHandler, Image } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, List, ListItem } from 'native-base'
import { connect } from 'react-redux'
import { Images, Metrics } from "../Themes";
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/SpecificationsScreenStyle'

class SpecificationsScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      ev: null
    };
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
    this.setState({
      ev: this.props.navigation.state.params.ev
    });
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  errorRender () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.icon} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Specifications</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <Text>Ops! An error has occurred!</Text>
        </Content>
      </Container>
    )
  }

  render () {
    if (this.state.ev === null) {
      return this.errorRender();
    }

    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.icon} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Specifications</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <Image source={{uri: this.state.ev.ev.avatar}} style={styles.image}/>
          <List style={styles.list}>
            <ListItem>
              <Left>
                <Text>Brand</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.brand}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Model</Text>
              </Left>
              <Text note>{this.state.ev.ev.model}</Text>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Year</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.year}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Charging Standard</Text>
              </Left>
              <Text note>{this.state.ev.ev.chargingStandard}</Text>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Weight</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.weight} kg</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Power</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.power} W</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Maximum Speed</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.maxSpeed} km/h</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Aerodynamic Coeficient</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.aerodynamicCoeficient}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Friction Coefficient</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.frictionCoefficient}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Battery</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.battery} kWh</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Decharge</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.decharge}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Charge</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.charge}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Moteur</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.moteur}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Precup</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.precup}</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Category</Text>
              </Left>
              <Right>
                <Text note>{this.state.ev.ev.category}</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecificationsScreen)
