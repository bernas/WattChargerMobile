import React, { Component } from 'react'
import { BackHandler } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PrivacySettingsScreenStyle'

class PrivacySettingsScreen extends Component {

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  render () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.icon} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>PrivacySettingsScreen</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <Text>PrivacySettingsScreen Content</Text>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivacySettingsScreen)
