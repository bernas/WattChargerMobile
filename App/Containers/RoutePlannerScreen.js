import React, { Component } from 'react'
import { Images, Metrics } from "../Themes";
import { BackHandler, Linking, Platform } from 'react-native'
import { Content,
          Container,
          Header,
          Left,
          Right,
          Body,
          Button,
          Text,
          Title,
          Icon,
          Footer,
          FooterTab,
          Thumbnail,
          Input,
          InputGroup,
          Item,
          Image,
          Fab,
          Spinner,
          Form,
          Toast,
          View } from 'native-base'
import Modal from 'react-native-modal'
import RouteMap from '../Components/RouteMap.js'
import SearchResults from '../Components/SearchResults.js'
import { Colors } from '../Themes'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import iterateAllPlans from '../Services/PolylineConversion.js'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import RNGooglePlaces from "react-native-google-places";

// Styles
var CHEAPEST_ROUTE_COLOR = '#99CC00';
var FASTEST_ROUTE_COLOR = '#FF4444';
var MO_FASTEST_ROUTE_COLOR = '#1c94c4';

var CHEAPEST_JOURNEY = "CHEAPEST_JOURNEY";
var FASTEST_JOURNEY = "FASTEST_JOURNEY";
var MO_FASTEST_JOURNEY = "MO_FASTEST_JOURNEY";
import styles from './Styles/RoutePlannerScreenStyle'

class RoutePlannerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: false,
      active: false,
      routeSelector: false,
      from: null,
      fromActive: false,
      to: null,
      toActive: false,
      routes: [],
      selectedRoutes: [],
      selectedChargers: [],
      showRoutingOptions: false,
      predictions: {},
      locations: [],
      searchFromText: "",
      searchToText: ""
    };
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack()
  };

  handlePressGreyNav = () => {
    this.setState({active: !this.state.active})
  }

  handlePressNav = () => {
    this.setState({showRoutingOptions: !this.state.showRoutingOptions})
  }

  handlePressAppleMaps = () => {
    var baseUrl = 'http://maps.apple.com/'
    var saddr = 'saddr='+this.state.from.latitude+','+this.state.from.longitude
    var dests = 'daddr='
    this.state.selectedChargers.map((charger,index) => {
      if (index==0) {
        dests= dests+charger.coordinates.latitude+','+charger.coordinates.longitude
      }
      else {
        dests = dests + '+to:' + charger.coordinates.latitude + ','+ charger.coordinates.longitude
      }
    })
    dests = dests+'+to:'+this.state.to.latitude+','+this.state.to.longitude
    var final_url = baseUrl+'?'+saddr+'&'+dests
    Linking.openURL(final_url)
  }

  handlePressGmaps = () => {
    if (Platform.OS === 'ios') {
      var baseUrl = 'comgooglemapsurl://http://maps.google.com/maps'
    }
    else {
      var baseUrl = 'http://maps.google.com/maps'
    }
    var saddr = 'saddr='+this.state.from.latitude+','+this.state.from.longitude
    var dests = 'daddr='
    this.state.selectedChargers.map((charger,index) => {
      if (index==0) {
        dests= dests+charger.coordinates.latitude+','+charger.coordinates.longitude
      }
      else {
        dests = dests + '+to:' + charger.coordinates.latitude + ','+ charger.coordinates.longitude
      }
    })
    dests = dests+'+to:'+this.state.to.latitude+','+this.state.to.longitude
    var final_url = baseUrl+'?'+saddr+'&'+dests
    Linking.openURL(final_url)
  }

  handlePressRouteSelector = () => {
    this.setState({routeSelector: !this.state.routeSelector})
  }

  handleFromInput = (text) => {
    this.setState({ searchFromText: text })
    RNGooglePlaces.getAutocompletePredictions(text, {country: 'DE'})
    .then(
      (res) => {
        this.setState({predictions: res})
        console.log(res)
      }
    )
    .catch(error => console.tron.log(error))
  }

  handleFromFocus = () => {
    this.setState({fromActive: true, toActive: false})
    if (this.state.searchFromText=="") {
      this.setState({predictions: {}})
    }
    if (this.state.to==null) {
      this.setState({searchToText: ""})
    }
  }


  createLatLng = (coordinates) => {
    var lat = coordinates.latE6 / 1E6;
    var lng = coordinates.lonE6 / 1E6;
    return {latitude: lat,longitude: lng};
  }

  handleFromSelection = (placeID) => {
    RNGooglePlaces.lookUpPlaceByID(placeID)
    .then(
      (res) => {
        console.tron.log(this.state)
        this.setState({predictions: {}})
        this._fromInput._root.blur()
        this.setState({fromActive: false})
        this.setState({searchFromText: res.address})
        this.setState({from: res})
        if (this.state.from!= null && this.state.to!=null) {
          data = {
            client:"journey-planner-for-EV",
            origin: {
              latE6:this.state.from.latitude * 1E6,
              lonE6:this.state.from.longitude * 1E6,
            },
            destination: {
              latE6: this.state.to.latitude * 1E6,
              lonE6: this.state.to.longitude * 1E6
            },
            stateOfChargeInPerc:50,
            centsPerHour:40,
          } 
          fetch('https://db1.umotional.net/charge-here/v1/journeys', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
          })
          .then((response) => {
            result = iterateAllPlans(response._bodyInit)
            if (result==null) {
              this.setState({fetching: false})
              Toast.show({
                text: 'Network Problem! Please try again later...',
                position: 'bottom',
                buttonText: 'Okay'
              })
            }
            else {
              this.setState({routes: result.routes, selectedRoutes: [result.routes[0]] , selectedChargers: result.markers[0], locations: result.markers})
              this.setState({fetching: false})
            }
          })
          this.setState({fetching: true})
        }
      }
    )
  }

  handleClearFrom = () => {
    this.setState({searchFromText: ""})
    this.setState({ from: null })
    this.setState({routes: [], selectedRoutes: [], locations: [], selectedChargers: [], showRoutingOptions: false})
  }

  handleToInput = (text) => {
    this.setState({ searchToText: text })
    RNGooglePlaces.getAutocompletePredictions(text,{country:'DE'})
    .then(
      (res) => {
        this.setState({predictions: res})
      }
    )
    .catch(error => console.tron.log(error))
  }

  handleToFocus = () => {
    this.setState({fromActive: false, toActive: true})
    if (this.state.searchToText=="") {
      this.setState({predictions: {}})
    }
    if (this.state.from==null) {
      this.setState({searchFromText: ""})
    }
  }

  handleRouteSelect = (route) => {
    this.setState({selectedRoutes: [this.state.routes[route]], selectedChargers: this.state.locations[route]})
  }

  handleToSelection = (placeID) => {
    RNGooglePlaces.lookUpPlaceByID(placeID)
    .then(
      (res) => {
        this.setState({predictions: {}})
        this._toInput._root.blur()
        this.setState({toActive: false})
        this.setState({to: res})
        this.setState({searchToText: res.address})
        if (this.state.from!= null && this.state.to!=null) {
          data = {
            client:"journey-planner-for-EV",
            origin: {
              latE6:this.state.from.latitude * 1E6,
              lonE6:this.state.from.longitude * 1E6,
            },
            destination: {
              latE6: this.state.to.latitude * 1E6,
              lonE6: this.state.to.longitude * 1E6
            },
            stateOfChargeInPerc:100,
            centsPerHour:40,
          } 
          fetch('https://db1.umotional.net/charge-here/v1/journeys', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
          })
          .then((response) => {
            result = iterateAllPlans(response._bodyInit)
            if (result==null) {
              this.setState({fetching: false})
              Toast.show({
                text: 'Network Problem! Please try again later...',
                position: 'bottom',
                buttonText: 'Okay'
              })
            }
            else {
              this.setState({routes: result.routes, selectedRoutes: [result.routes[0]], selectedChargers: result.markers[0], locations: result.markers})
              this.setState({fetching: false})
            }
          })
          this.setState({fetching: true})
        }
      }
    )
  }

  dragFromCallback = async (location) => {
    try{
      location.persist();
      const myApiKey = 'AIzaSyCHWDktJU_tl81KOq8RehgjPanHGuwt-qI';
      const latitude = location.nativeEvent.coordinate.latitude;
      const longitude = location.nativeEvent.coordinate.longitude;
      const result = await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + latitude + ',' + longitude + '&key=' + myApiKey);
      const json = await result.json();
      console.tron.log(json);
      this.handleFromSelection(json.results[0].place_id);
    }
    catch(err){
      Toast.show({
        text: err,
        position: 'bottom',
        buttonText: 'Okay',
        duration: 2000
      })
    }
  }

  dragToCallback = async (location) => {
    try {
      location.persist();
      const myApiKey = 'AIzaSyCHWDktJU_tl81KOq8RehgjPanHGuwt-qI';
      const latitude = location.nativeEvent.coordinate.latitude;
      const longitude = location.nativeEvent.coordinate.longitude;
      const result = await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + latitude + ',' + longitude + '&key=' + myApiKey);
      const json = await result.json();
      console.tron.log(json);
      this.handleToSelection(json.results[0].place_id);
    }
    catch (err) {
      Toast.show({
        text: err,
        position: 'bottom',
        buttonText: 'Okay',
        duration: 2000
      })
    }
  }

  handleClearTo = () => {
    this.setState({searchToText: ""})
    this.setState({ to: null });
    this.setState({routes: [], selectedRoutes: [], locations: [], selectedChargers: [], showRoutingOptions: false })
  }

  handleResultsHide = () => {
    if (this.state.fromActive) {
      this.setState({fromActive: false})
      if (this.state.from==null) {
        this.setState({searchFromText: ""})
      }
      this._fromInput._root.blur()
    }
    if (this.state.toActive) {
      this.setState({toActive: false})
      if (this.state.to==null) {
        this.setState({searchToText: ""})
      }
      this._toInput._root.blur()
    }
  }



  render () {
    const { searchFromText, searchToText } = this.state
    return (
      <Container>
        <Header searchBar style={styles.header} >
          <Left/>
          <Body >
            <Form style={styles.form} >
              <Item >
                <FontAwesome name="dot-circle-o" style={{fontSize: 20, color: 'white'}} />
                <Input 
                ref={ ref => (this._fromInput) = ref}
                autoCapitalize="none" 
                autoCorrect={false} 
                placeholderTextColor={Colors.ricePaper} 
                placeholder="From..." 
                value={searchFromText}
                onChangeText={ this.handleFromInput }
                onFocus= {this.handleFromFocus}
                style={{color: 'white'}}
                />
                { !(this.state.from==null) && <Right>
                  <Button transparent onPress={this.handleClearFrom}>
                    <Icon name="md-close" style={{fontSize: 20, color: Colors.ricePaper}}/>
                  </Button>
                </Right> }
              </Item>
              <Item>
                <FontAwesome name="flag-checkered" style={{fontSize: 20, color: 'white'}} />
                <Input 
                ref={ ref => (this._toInput) = ref}
                autoCapitalize="none" 
                autoCorrect={false} 
                placeholderTextColor={Colors.ricePaper}  
                placeholder="To..." 
                value={searchToText}
                onChangeText = {this.handleToInput}
                onFocus = {this.handleToFocus}
                style={{color: 'white'}}
                />
                { !(this.state.to==null) && <Right>
                  <Button transparent onPress={this.handleClearTo}>
                    <Icon name="md-close" style={{fontSize: 20, color: Colors.ricePaper}}/>
                  </Button>
                </Right> }
              </Item>
            </Form>
          </Body>
          <Right>
            { (this.state.fromActive || this.state.toActive ) && <Button transparent onPress={this.handleResultsHide}>
              <Icon name="md-arrow-down" style={{color: Colors.ricePaper}}/>
            </Button> }
          </Right>
        </Header>
        <Content automaticallyAdjustContentInsets={false} padder style={styles.mapContainer}>
          <Container>
            <RouteMap 
                      routes={this.state.selectedRoutes} 
                      locations={this.state.selectedChargers} 
                      from={this.state.from} 
                      to={this.state.to} 
                      fromCallback={this.dragFromCallback}
                      toCallback={this.dragToCallback}
                      />
          </Container>
        </Content>
        { (this.state.routes.length>0) && <Fab
          containerStyle={{paddingBottom:170}}
          active={this.state.routeSelector}
          direction="right"
          style={{ backgroundColor: 'white' }}
          position="topLeft"
          onPress={this.handlePressRouteSelector}>
          <Icon name='md-menu' style={{color: 'black'}} />
            { this.state.routes.map((route,key) => {
              switch(route.type) {
                case CHEAPEST_JOURNEY:
                    var color = CHEAPEST_ROUTE_COLOR
                    var icon = "currency-eur"
                    break;
                case FASTEST_JOURNEY:
                    var color = FASTEST_ROUTE_COLOR
                    var icon = "speedometer"
                    break;
                case MO_FASTEST_JOURNEY:
                    var color = MO_FASTEST_ROUTE_COLOR
                    var icon = "call-split"
                    break;
                default:
                    var color = "#000"
              }
              return(<Button key={key} onPress={this.handleRouteSelect.bind(this,key)} style={{marginTop:115, flex: 1, flexDirection: 'column-reverse', backgroundColor: color}}>
                <MaterialCommunityIcons name={icon} style={{fontSize: 20, color: 'white'}} />
                </Button>)
                }) 
            }
          </Fab> }
        <Fab
          active={this.state.active}
          direction="up"
          style={{ backgroundColor: 'crimson' }}
          position="bottomLeft"
          onPress={this.handlePressBack}>
          <Icon name='md-close' />
        </Fab>
        { (this.state.selectedRoutes.length>0) && <Fab
          style={{flex: 1}}
          active={this.state.showRoutingOptions}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: 'dodgerblue' }}
          position="bottomRight"
          onPress={this.handlePressNav}>
          <Icon name='md-navigate' />
          <Button style={{ backgroundColor: '#34A34F' }} onPress={this.handlePressGmaps}>
              <MaterialCommunityIcons name="google-maps" style={{fontSize: 20, color: 'white'}} />
            </Button>
            <Button style={{ backgroundColor: '#3B5998' }} onPress={this.handlePressAppleMaps}>
              <MaterialCommunityIcons name="apple" style={{fontSize: 20, color: 'white'}} />
            </Button>
        </Fab> }
        { (this.state.selectedRoutes.length==0) && <Fab
          style={{flex: 1}}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: 'grey' }}
          position="bottomRight"
          onPress={this.handlePressGreyNav}>
          <Icon name='md-navigate' />
        </Fab> }
      {this.state.fromActive && <SearchResults callback={this.handleFromSelection} predictions={{inText: this.state.searchFromText, pred: this.state.predictions}} style={styles.searchResultsWrapper}/>}
      {this.state.toActive && <SearchResults callback={ this.handleToSelection } predictions={{inText: this.state.searchToText, pred: this.state.predictions}} style={styles.searchResultsWrapper}/>}
      
      <Modal animationIn='bounceIn' isVisible={this.state.fetching} backdropOpacity={0}>
		    <Spinner color={Colors.background}/>
      </Modal>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoutePlannerScreen)
