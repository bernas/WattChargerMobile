import React, { PropTypes, Component } from 'react'
import { BackHandler, Keyboard, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native'
import { Images, Metrics, Colors } from "../Themes";
import Entypo from 'react-native-vector-icons/Entypo'
import { Toast, View, Label, Item, Input, Form, Content, Container, Header, Left, Right, Body, Button, Text, Icon, Title,  Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import RegisterActions from "../Redux/RegisterRedux";
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

// Styles
import styles from './Styles/RegisterScreenStyle'

class RegisterScreen extends React.Component {
  static propTypes = {
		dispatch: PropTypes.func,
		fetching: PropTypes.bool,
	};

	isAttempting = false;
	keyboardDidShowListener = {};
	keyboardDidHideListener = {};


	constructor(props) {
		super(props);
		this.state = {
      name: "",
			username: "",
      password: "",
      passwordver: "",
      email: "",
			visibleHeight: Metrics.screenHeight,
			topLogo: { width: Metrics.screenWidth - 40 },
		};
		this.isAttempting = false;
	}

	componentWillReceiveProps(newProps) {
		this.forceUpdate();
		// Did the login attempt complete?
		if (this.isAttempting && !newProps.fetching) {
			//this.props.navigation.goBack();
			this.props.navigation.navigate("WelcomeScreen");
			Toast.show({
				text: 'Register succeded',
				position: 'top',
				buttonText: 'Okay',
				duration: 2000
			})
		}
	}

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      //this.props.navigation.goBack();
      this.props.navigation.gpBack();
      return true
    })
  }

  handlePressBack = () => {
		// const { username, password } = this.state
		// this.isAttempting = true
		// attempt a login - a saga is listening to pick it up from here.
		// this.props.attemptLogin(username, password);
		this.props.navigation.goBack();
  };

	handlePressRegister = () => {
		const { name, username, password, passwordver, email } = this.state
		this.props.registerVerification({
			variables: {
        name,
        username,
        password,
        email
			}
		}).then( res => { 
			this.props.attemptRegister(res);
			})
			.catch(res => {
				const errors = res.graphQLErrors.map(error => error.message);
				Toast.show({
					text: errors,
					position: 'top',
					buttonText: 'Okay',
					duration: 2000
				})
			}
		)
		this.isAttempting = true
  };

  handleChangeName = text => {
    this.setState({ name: text });
  };

	handleChangeUsername = text => {
		this.setState({ username: text });
  };

	handleChangePassword = text => {
		this.setState({ password: text });
  };

	handleCheckPassword = text => {
    this.setState({ passwordver: text});
	};

	handleChangeEmail = text => {
		this.setState({ email: text });
	};

  render () {
		const { name, username, password, passwordver, email } = this.state;
		const { fetching } = this.props;
		const editable = !fetching;
    return (
      <Container>
        <Header style={styles.header}>
          <Body>
            <Title style={{color: 'white'}}>REGISTER</Title>
          </Body>
        </Header>
        <Content padder style={styles.container} extraScrollHeight={55}>
          <Form style={styles.form}>
            <Item regular style={styles.formInput}> 
              <Icon active name='md-person' style={{color:'white'}}/>
              <Label placeholderLabel style={styles.labelText}>Name</Label>
							<Input
								ref="name"
                value={name}
								editable={editable}
								keyboardType="default"
								returnKeyType="next"
								autoCorrect={false}
								style={{color: 'white'}}
								onChangeText={this.handleChangeName}
								underlineColorAndroid="transparent"
								onSubmitEditing={() => this.username._root.focus()}
							/>
            </Item>
            <Item regular style={styles.formInput}> 
              <Icon active name='md-person' style={{color:'white'}}/>
              <Label style={styles.labelText}>Username</Label>
							<Input
								ref={ ref => (this.username = ref)}
                value={username}
								editable={editable}
								keyboardType="default"
								returnKeyType="next"
								autoCapitalize="none"
								autoCorrect={false}
								style={{color: 'white'}}
								onChangeText={this.handleChangeUsername}
								underlineColorAndroid="transparent"
								onSubmitEditing={() => this.email._root.focus()}
							/>
            </Item>
            <Item regular style={styles.formInput}>
              <Icon active name='md-mail' style={{color:'white'}}/>
              <Label style={styles.labelText}>E-mail</Label>
							<Input
								ref= { ref => (this.email = ref) }
                value={email}
								editable={editable}
								keyboardType="email-address"
								returnKeyType="next"
								autoCapitalize="none"
								autoCorrect={false}
								style={{color: 'white'}}
								onChangeText={this.handleChangeEmail}
								underlineColorAndroid="transparent"
								onSubmitEditing={() => this.password._root.focus()}
							/>
            </Item>
            <Item regular style={styles.formInput}>
              <Icon active name='md-lock' style={{color:'white'}}/>
              <Label style={styles.labelText}>Password</Label>
							<Input
								ref={ref => (this.password = ref)}
								value={password}
								editable={editable}
								keyboardType="default"
								returnKeyType="next"
								autoCapitalize="none"
								autoCorrect={false}
								style={{color: 'white'}}
								onChangeText={this.handleChangePassword}
								secureTextEntry
								underlineColorAndroid="transparent"
								onSubmitEditing={() => this.passwordver._root.focus()}
							/>
            </Item>
            <Item regular style={styles.formInput}>
              <Icon active name='md-lock' style={{color:'white'}}/>
              <Label style={styles.labelText}>Repeat Password</Label>
							<Input
                ref={ref => (this.passwordver = ref)}
                value={passwordver}
								editable={editable}
								keyboardType="default"
								returnKeyType="go"
								autoCapitalize="none"
								autoCorrect={false}
								style={{color: 'white'}}
								onChangeText={this.handleCheckPassword}
								secureTextEntry
								underlineColorAndroid="transparent"
								onSubmitEditing={this.handlePressRegister}
							/>
            </Item>
          </Form>
          <View style={[styles.loginRow]}>
            <Button active block style={{ backgroundColor: '#3B5998' }} onPress={this.handlePressRegister}>
              <Icon name="logo-facebook" />
              <Text>Register with Facebook</Text>
            </Button>
          </View>
          <View style={[styles.loginRow]}>
            <Button active block style={{ backgroundColor: '#DD4B39' }} onPress={this.handlePressRegister}>
              <Icon name="logo-google" />
              <Text>Register with Google</Text>
            </Button>
          </View>
        </Content>
				{ (Platform.OS === 'ios') ? 
				<KeyboardAvoidingView behavior='padding'>
        <Footer>
          <FooterTab>
            <Button active danger full onPress={this.handlePressBack}>
              <Text style={{ color: 'white' }} >CANCEL</Text>
            </Button>
          </FooterTab>
					<FooterTab>
						<Button active success full onPress={this.handlePressRegister}>
						  <Text style={{color: 'white'}}>REGISTER</Text>
						</Button>
					</FooterTab>
        </Footer>
				</KeyboardAvoidingView>
				:
        <Footer>
          <FooterTab>
            <Button active danger full onPress={this.handlePressBack}>
              <Text style={{ color: 'white' }} >CANCEL</Text>
            </Button>
          </FooterTab>
					<FooterTab>
						<Button active success full onPress={this.handlePressRegister}>
						  <Text style={{color: 'white'}}>REGISTER</Text>
						</Button>
					</FooterTab>
        </Footer>
		  }
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
	return {
		fetching: state.register.fetching,
	};
}

const mapDispatchToProps = (dispatch) => {
  return {
		attemptRegister: (token) => dispatch(RegisterActions.registerRequest(token)),
  }
}

const UserRegisterQuery = gql`
mutation SignupUserMutation($name: String!, $username: String!, $email: String!, $password: String!) {
  signup(
    name: $name, 
    username: $username, 
    email: $email, 
    password: $password
  ) {
    token
  }
}
`

export default compose(
	graphql(UserRegisterQuery, { name: 'registerVerification' }),
	connect(mapStateToProps, mapDispatchToProps))(RegisterScreen);
