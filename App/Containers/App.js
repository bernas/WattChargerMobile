import '../Config'
import DebugConfig from '../Config/DebugConfig'
import React, { Component } from 'react'
import ApolloClient, { ApolloProvider, createNetworkInterface } from 'react-apollo';
import RootContainer from './RootContainer'
import { Root } from 'native-base'
import createStore from '../Redux'

// create our store
const store = createStore()

const networkInterface = createNetworkInterface({
    uri: 'https://wattcharger-api.herokuapp.com/api'
  })

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};
    }
    // get the authentication token from local storage if it exists
    const token = store.getState().login.token;
    const transientToken = store.getState().transientlogin.token;
    if (token!=null) {
      req.options.headers.authorization = `Bearer ${token.data.login.token}`;
    }
    else if (transientToken!=null) {
      req.options.headers.authorization = `Bearer ${transientToken.data.login.token}`;
    }
    console.tron.display({
      name: 'Apollo Client',
      preview: 'Apollo Header',
      value: {
        transientToken,
        req
      }
    })
    next();
  },
}]);


export const client = new ApolloClient({
  networkInterface
});

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  render () {
    return (
      <ApolloProvider client={client} store={store}>
        <Root><RootContainer /></Root>
      </ApolloProvider>
    )
  }
}

export default DebugConfig.useReactotron
? console.tron.overlay(App)
: App
