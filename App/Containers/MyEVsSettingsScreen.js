import React, { Component } from 'react'
import { BackHandler, Image, RefreshControl } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, List, ListItem, Card, CardItem, Spinner } from 'native-base'
import { connect } from 'react-redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import { Images, Metrics } from "../Themes";
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/MyEVsSettingsScreenStyle'

class MyEVsSettingsScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  fetchData = async() => {
    this.props.getUserEvs.refetch()
  };

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  handleSelectedEV = () => {
    this.props.getUserEvs.refetch()
  };

  handlePressAddEv = () => {
    this.props.navigation.navigate("AddEvScreen", {handlerFunction: this.handleSelectedEV});
  };

  handlePressSpecifications = (ev) => {
    this.props.navigation.navigate("SpecificationsScreen", {ev: ev});
  };

  handlePressRemove = async(ev) => {
    const ev_id = parseInt(ev.id)

    const answer = await this.props.deleteEv({
      variables: {
        id: ev_id
      }
    })

    this.props.getUserEvs.refetch()
  };

  createMap = (ev) => {
    return (
      <Card key={ev.name}>
        <CardItem header>
          <Left>
            <Body>
              <Text>{ev.ev.brand} {ev.ev.model}</Text>
              <Text note style={{width: 200}}>See Specifications</Text>
            </Body>
          </Left>
          <Right>
            <Button transparent onPress={this.handlePressSpecifications.bind(this, ev)}>
              <Icon style={styles.iconContainer} name="md-arrow-forward" />
            </Button>
          </Right>
        </CardItem>
        <CardItem cardBody>
          <Image source={{uri: ev.ev.avatar}} style={styles.image} />
        </CardItem>
        <CardItem footer>
          <Left />
          <Right>
            <Button transparent onPress={this.handlePressRemove.bind(this, ev)}>
              <Text note>Remove </Text>
              <Icon style={styles.iconContainer} name="md-trash" />
            </Button>
          </Right>
        </CardItem>
      </Card>
    )
  }

  createCars = () => {
    if (this.props.getUserEvs.getEvsFromUser.length === 0) {
      return (
        <Container>
          <Button full iconLeft transparent onPress={this.handlePressAddEv}>
            <Icon style={styles.iconContainer} name="md-add" />
            <Text note>Add a Vehicle</Text>
          </Button>
        </Container>
      )
    } else {
      return this.props.getUserEvs.getEvsFromUser.map((ev) => this.createMap(ev))
    }
  }

  loadingRender () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>My EVs</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.handlePressAddEv}>
              <Icon style={styles.iconHeader} name="md-add" />
            </Button>
          </Right>
        </Header>
        <Content padder style={styles.container}>
          <Spinner color='#016976' />
        </Content>
      </Container>
    )
  }

  errorRender () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>My EVs</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.handlePressAddEv}>
              <Icon style={styles.iconHeader} name="md-add" />
            </Button>
          </Right>
        </Header>
        <Content padder style={styles.container}>
          <Text>Ops! An error has occurred!</Text>
        </Content>
      </Container>
    )
  }

  render () {
    if (this.props.getUserEvs && this.props.getUserEvs.loading)
      return this.loadingRender();

    if (this.props.getUserEvs && this.props.getUserEvs.error)
      return this.errorRender();

    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.iconHeader} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>My EVs</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.handlePressAddEv}>
              <Icon style={styles.iconHeader} name="md-add" />
            </Button>
          </Right>
        </Header>
        <Content padder style={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          { this.createCars() }
          <Text></Text>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const USER_EVS_QUERY = gql`
  query UserEvsQuery {
    getEvsFromUser {
      ev {
        avatar,
        brand,
        chargingStandard,
        maxCharge,
        model,
        year,
        weight,
        power,
        maxSpeed,
        aerodynamicCoeficient,
        frictionCoefficient,
        battery,
        decharge,
        charge,
        moteur,
        precup,
        category
      },
      id,
      consumption,
      savings,
      name
    }
  }
`

const REMOVE_EV_MUTATION = gql`
  mutation RemoveEvMutation($id: Int!) {
    deleteEvToUser(id: $id){ id }
  }
`

export default compose(
  graphql(USER_EVS_QUERY, { name: 'getUserEvs' }),
  graphql(REMOVE_EV_MUTATION, { name: 'deleteEv' }),
  connect(mapStateToProps, mapDispatchToProps)
)(MyEVsSettingsScreen)
