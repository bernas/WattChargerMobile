import React, { Component } from 'react'
import { BackHandler, Switch } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, List, ListItem } from 'native-base'
import { connect } from 'react-redux'
import { Colors } from '../Themes/'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AccountSettingsScreenStyle'

class AccountSettingsScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {switchValue: true};
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  _onPressButton() {
    if(this.state.switchValue == true)
      this.setState({ switchValue: false });
    else
      this.setState({ switchValue: true });
  }

  render () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={{color: 'white'}} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>AccountSettingsScreen</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <List style={styles.list}>
            <ListItem icon onPress={this.handlePressProfileSettings}>
              <Left>
                <Icon style={{color: Colors.steel}} name="md-person" />
              </Left>
              <Body>
                <Text>Private Profile</Text>
              </Body>
              <Right>
                <Switch
                  onValueChange={this._onPressButton.bind(this)}
                  value={this.state.switchValue }
                />
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountSettingsScreen)
