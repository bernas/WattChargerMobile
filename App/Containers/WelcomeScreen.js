import React, { PropTypes, Component } from 'react'
import { BackHandler, Keyboard, Image } from 'react-native'
import { Images, Metrics } from "../Themes";
import LoginActions from "../Redux/LoginRedux";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { connect } from 'react-redux'
import { View, Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, LayoutAnimation, DeckSwiper, Card, CardItem} from 'native-base'
import SliderEntry from '../Components/SliderEntryWelcome'

import LoginBackground from '../Components/LoginBackground.js'

// Styles
import styles from './Styles/WelcomeScreenStyle'
import { sliderWidth, itemWidth } from '../Components/Styles/SliderEntryStyle';

var ENTRIES1 = [
  {
      title: 'Find Chargers',
      subtitle: 'Find a charger compatible with your EV.',
  },
  {
      title: 'Create Routes',
      subtitle: 'Create a route tailored-made for your EV\'s specifications.',
  },
  {
      title: 'Social',
      subtitle: 'Interact and share your trips with other EV enthusiasts.'
  },
  {
      title: 'Statistics',
      subtitle: 'To be determined...'
  },
];

class WelcomeScreen extends Component {
	static propTypes = {
		dispatch: PropTypes.func,
		fetching: PropTypes.bool,
		attemptLogin: PropTypes.func,
	};


  _renderItemWithParallax ({item, index}, parallaxProps) {
    return (
        <SliderEntry
          data={item}
          even={(index + 1) % 2 === 0}
          parallax={false}
        />
    );
  }

	isAttempting = false;
	keyboardDidShowListener = {};
	keyboardDidHideListener = {};


	constructor(props) {
		super(props);
		this.state = {
			slider1ActiveSlide: 0,
			username: "",
			password: "",
			visibleHeight: Metrics.screenHeight,
			topLogo: { width: Metrics.screenWidth - 40 },
		};
		this.isAttempting = false;
		if (this.props.token != null) {
			this.props.attemptLogin(this.props.token);
		}
	}

	componentWillReceiveProps(newProps) {
		this.forceUpdate();
		// Did the login attempt complete?
		if (this.isAttempting && !newProps.fetching) {
			this.props.navigation.goBack();
		}
		if (newProps.token != null) {
			this.props.attemptLogin(newProps.token);
		}
	}

	handlePressLogin = () => {
		this.props.navigation.navigate("LoginScreen");
	};

	handlePressRegister = () => {
		this.props.navigation.navigate("RegisterScreen");
	};

	handleChangeUsername = text => {
		this.setState({ username: text });
	};

	handleChangePassword = text => {
		this.setState({ password: text });
	};

	render() {
		const { username, password } = this.state;
		const { fetching } = this.props;
		const editable = !fetching;
    return (
      <Container>
        <Content scrollEnabled={false}>
          <Container style={styles.video}><LoginBackground /></Container>
					<Image style={styles.logo} source={Images.alternateLogoWhite} />
          <Carousel
            ref={c => this._slider1Ref = c}
            data={ENTRIES1}
            renderItem={this._renderItemWithParallax}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            hasParallaxImages={false}
            firstItem={0}
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0}
            containerCustomStyle={styles.slider}
            contentContainerCustomStyle={styles.sliderContentContainer}
            loop={false}
            loopClonesPerSide={2}
            autoplay={false}
            autoplayDelay={500}
            autoplayInterval={3000}
            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
          />
          <Pagination
            dotsLength={ENTRIES1.length}
            activeDotIndex={this.state.slider1ActiveSlide}
            containerStyle={styles.paginationContainer}
            dotColor='white'
            dotStyle={styles.paginationDot}
            inactiveDotColor='white'
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={this._slider1Ref}
            tappableDots={!!this._slider1Ref}
          />
        </Content>
        <Footer>
          <FooterTab>
            <Button active primary full onPress={this.handlePressLogin}>
              <Text style={{ color: 'white' }} >LOG IN</Text>
            </Button>
            <Button active info full onPress={this.handlePressRegister}>
              <Text style={{ color: 'white' }} >REGISTER</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = state => {
	return {
		token: state.login.token,
		fetching: state.login.fetching,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		attemptLogin: (token) => dispatch(LoginActions.loginRequest(token)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen)
