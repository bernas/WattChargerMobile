import React, { Component } from 'react'
import { BackHandler } from 'react-native'
import { Content, Container, Header, List, ListItem, Switch, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab } from 'native-base'
import { Col, Row, Grid } from "react-native-easy-grid"
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import LoginActions from "../Redux/LoginRedux";

// Styles
import styles from './Styles/SettingsScreenStyle'

class SettingsScreen extends Component {

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

	constructor(props) {
		super(props);
  }

	componentWillReceiveProps(newProps) {
		this.forceUpdate();
		// Did the login attempt complete?
		if (this.isAttempting && !newProps.fetching) {
			this.props.navigation.goBack();
		}
	}

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

	handlePressMap = () => {
		this.props.navigation.navigate("MapScreen");
  };

  handlePressSocial = () => {
    this.props.navigation.navigate("SocialScreen");
  };

  handlePressLogOut = () => {
    this.props.logout();
  };

  handlePressProfileSettings = () => {
    this.props.navigation.navigate("ProfileSettingsScreen");
  };

  handlePressMyEVsSettings = () => {
    this.props.navigation.navigate("MyEVsSettingsScreen");
  };

  handlePressMapNavSettings = () => {
    this.props.navigation.navigate("MapNavSettingsScreen");
  };


  handlePressAccountSettings = () => {
    this.props.navigation.navigate("AccountSettingsScreen");
  };

  render () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title style={styles.title}>Settings</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
          <List style={styles.list}>
            <ListItem icon onPress={this.handlePressProfileSettings}>
              <Left>
                <Icon style={styles.icon} name="md-person" />
              </Left>
              <Body>
                <Text>Profile</Text>
              </Body>
              <Right>
                <Icon name="md-arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon onPress={this.handlePressMyEVsSettings}>
              <Left>
                <Icon style={styles.icon} name="md-car" />
              </Left>
              <Body>
                <Text>My EVs</Text>
              </Body>
              <Right>
                <Icon name="md-arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon onPress={this.handlePressMapNavSettings}>
              <Left>
                <Icon style={styles.icon} name="md-pin" />
              </Left>
              <Body>
                <Text>Map Settings</Text>
              </Body>
              <Right>
                <Icon name="md-arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon onPress={this.handlePressAccountSettings}>
              <Left>
                <Icon style={styles.icon} name="md-options" />
              </Left>
              <Body>
                <Text>Account</Text>
              </Body>
              <Right>
                <Icon name="md-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          <Button block danger onPress={this.handlePressLogOut} style={{top: 20}}>
            <Text>Log Out</Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
		logout: () => dispatch(LoginActions.logout()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen)
