import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  loginRow: {
    paddingBottom: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flex: 1,
    flexDirection: 'column',
    marginTop: 10
  },
  container: {
    paddingTop: 10,
    backgroundColor: Colors.background
  },
  row: {
    paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flexDirection: 'row',
    flex: 1
  },
  rowLabel: {
    color: Colors.charcoal
  },
  input: {
    backgroundColor: Colors.cloud,
    marginBottom: 20
  },
  labelText: {
    color: Colors.ricePaper
  },
  textInputReadonly: {
    color: Colors.steel
  },
  loginRow: {
    paddingBottom: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flexDirection: 'row',
    marginTop: 10
  },
  loginButtonWrapper: {
    flex: 1
  },
  loginButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.backgroundButton,
    backgroundColor: Colors.backgroundButton,
    padding: 6
  },
  cancelButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.panther,
    backgroundColor: Colors.panther,
    padding: 6
  },
  loginText: {
    textAlign: 'center',
    color: Colors.silver
  },
  logoContainer: {
    alignSelf: 'center',
    flexGrow: 1,
  },
  logo: {
    width: Metrics.screenWidth - 50,
    height: Metrics.screenHeight - 400,

    resizeMode: 'contain'
  }
})
