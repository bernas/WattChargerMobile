import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  title: {
    color: 'white'
  },

  iconHeader: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  iconContainer: {
    color: Colors.steel
  },

  image: {
    height: 175,
    width: null,
    flex: 1,
    resizeMode: 'contain'
  }

})
