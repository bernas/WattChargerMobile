import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
import { Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingTop: 10,
    backgroundColor: Colors.background
  },
  header: {
    backgroundColor: Colors.background,
    borderBottomWidth: 0
  },
  loginRow: {
    paddingBottom: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flex: 1,
    flexDirection: 'column',
    marginTop: 10
  },
  labelText: {
    color: Colors.ricePaper 
  },
  cancelButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.panther,
    backgroundColor: Colors.panther,
    padding: 6
  },
  form: {
    marginBottom: 35,
  },
  formInput: {
    marginBottom: 15
  },
  formLabelContainer: {
    alignSelf: 'center',
    flex: 1,

  },
  formLabel: {
    color: Colors.ricePaper
  }
})
