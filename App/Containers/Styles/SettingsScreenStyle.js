import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  title: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  list: {
    width: Metrics.screenWidth - 25
  },

  icon: {
    color: Colors.steel
  }

})
