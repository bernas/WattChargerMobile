import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics,Colors } from '../../Themes/'

export default StyleSheet.create({
    ... ApplicationStyles.screen,

    header: {
      backgroundColor: Colors.background
    },

    headerTitle: {
      color: 'white'
    },

    container: {
      backgroundColor: 'white'
    },


    icon: {
      color: 'white'
    },

    title: {
      paddingHorizontal: 30,
      backgroundColor: 'transparent',
      color: 'rgba(255, 255, 255, 0.9)',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    subtitle: {
        marginTop: 5,
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.75)',
        fontSize: 13,
        fontStyle: 'italic',
        textAlign: 'center'
    },
    slider: {
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    }

})
