import { Dimensions,StyleSheet } from 'react-native'
import { ApplicationStyles , Colors, Metrics } from '../../Themes/'
var width = Dimensions.get("window").width; //full width

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background,
    height: 130
  },
  routeSelector: {
    height: 130
  },
  body: {
    flex: -1
  },
  backButton: {
    flex: -10
  },
  form: {
    flexDirection: 'column',
    width: Metrics.screenWidth - (Metrics.screenWidth/3) 
  },
  input: {
    color: 'white',
    textShadowColor:'#FFFFFF',
  },
  mapContainer: {
    marginLeft: -Metrics.baseMargin,
    marginRight: -Metrics.baseMargin,
    marginBottom: 0,
    marginTop: -10
  },
  searchResultsWrapper:{
      top: 130,
      position:"absolute",
      width:width,
      height:1000,
      backgroundColor:"#fff",
      opacity:0.9
  },


})
