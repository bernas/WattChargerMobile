import { Dimensions, StyleSheet } from 'react-native'
import { Colors } from '../../Themes'
import { ApplicationStyles } from '../../Themes/'
var width = Dimensions.get("window").width; //full width

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  topLeftButton: {
    resizeMode: 'contain'
  },
  header: {
    backgroundColor: Colors.background,
  },
  leftHeaderContainer: {
    flex: -1,
  },
  bodyHeaderContainer: {
    flex: 1,
  },
  rightHeaderContainer: {
    flex: -1,
  },
  searchBar: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
  },
  searchResultsWrapper:{
      top: 60,
      position:"absolute",
      width:width,
      height:1000,
      backgroundColor:"#fff",
      opacity:0.9
  },
  topRightButton: {

  },
  carthumb: {
    resizeMode: 'contain',
  },
})
