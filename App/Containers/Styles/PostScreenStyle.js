import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  title: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  list: {
    width: Metrics.screenWidth - 35
  },

  icon: {
    color: Colors.steel
  },
  
  postCommentWrapper:{
    justifyContent: "flex-end",
    margin: 0,
    marginBottom: 55
  },
  topCard: {
    flex: -1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  card: {
    borderRadius: 20
  },
})
