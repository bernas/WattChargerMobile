import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  title: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  icon: {
    color: 'white'
  }
  
})
