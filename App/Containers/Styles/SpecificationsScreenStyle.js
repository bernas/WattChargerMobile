import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors } from '../../Themes/'

export default StyleSheet.create({
  ... ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  title: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  image: {
    height: 175,
    width: Metrics.screenWidth - 50,
    alignSelf: "center",
    resizeMode: 'contain'
  },

  list: {
    width: Metrics.screenWidth - 35,
    paddingBottom: Metrics.doubleBaseMargin,
    backgroundColor: 'white'
  },

  icon: {
    color: 'white'
  }

})
