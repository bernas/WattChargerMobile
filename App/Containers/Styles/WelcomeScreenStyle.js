import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    zIndex: 1,
  },
  rest: {
    zIndex: 2
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  logo: {
    width: Metrics.screenWidth - 50,
    height: Metrics.screenHeight - 600,
    alignSelf: 'center',
    flexGrow: 1,
    resizeMode: 'contain',
    marginTop: 50,
    paddingBottom: 60
  },
  paginationContainer: {
      paddingVertical: -2
  },
})
