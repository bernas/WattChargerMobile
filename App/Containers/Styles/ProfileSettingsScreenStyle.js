import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  header: {
    backgroundColor: Colors.background
  },

  iconHeader: {
    color: 'white'
  },

  title: {
    color: 'white'
  },

  container: {
    backgroundColor: 'white'
  },

  thumbnail: {
    height: 150,
    width: 150,
    flex: 1,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 40,
    marginBottom: 50,
  },

  item: {
    marginBottom: 5
  },

  iconItem: {
    color: Colors.steel
  },

  label: {
    color: 'black'
  }

})
