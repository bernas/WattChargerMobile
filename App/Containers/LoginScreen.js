import React, { PropTypes } from "react";
import { BackHandler, Image, Keyboard, LayoutAnimation, KeyboardAvoidingView, Platform } from "react-native";
import Modal from 'react-native-modal'
import { connect } from "react-redux";
import Styles from "./Styles/LoginScreenStyles";
import { Images, Metrics } from "../Themes";
import LoginActions from "../Redux/LoginRedux";
import TransientLoginActions from "../Redux/TransientLoginRedux";
import { graphql, compose } from 'react-apollo'
import { client } from './App'
import gql from 'graphql-tag'
import { Toast, View, Right, Title, Body, Left, Header, Content, Container, Icon, Button, Text, Contant, CheckBox, Form, Item, Input, Label, Thumbnail, Footer, FooterTab, Spinner } from "native-base";

class LoginScreen extends React.Component {
	static propTypes = {
		dispatch: PropTypes.func,
		fetching: PropTypes.bool,
		attemptLogin: PropTypes.func,
	};

	isAttempting = false;
	keyboardDidShowListener = {};
	keyboardDidHideListener = {};

	constructor(props) {
		super(props);
		this.state = {
			username: "",
			password: "",
			remember: false,
			visibleHeight: Metrics.screenHeight,
			// Change this variables to change the size of the logo
		};
		this.isAttempting = false;
	}

	componentWillReceiveProps(newProps) {
		this.forceUpdate();
		// Did the login attempt complete?
		if (this.isAttempting && !newProps.fetching) {
			this.props.navigation.goBack();
			//this.props.navigation.navigate("WelcomeScreen");
		}
	}
	componentDidMount () {
	  BackHandler.addEventListener('hardwareBackPress', () => {
	    //this.props.navigation.goBack();
	    this.props.navigation.navigate("WelcomeScreen");
	    return true
	  })
	}

	componentWillMount() {
		// Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
		// TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
		this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide);
	}

	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	handlePressBack = () => {
		// const { username, password } = this.state
		// this.isAttempting = true
		// attempt a login - a saga is listening to pick it up from here.
		// this.props.attemptLogin(username, password);
		this.props.navigation.goBack();
  	};

	keyboardDidShow = e => {
		// Animation types easeInEaseOut/linear/spring
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		let newSize = Metrics.screenHeight - e.endCoordinates.height;
		this.setState({
			visibleHeight: newSize,
			topLogo: { width: 100, height: 70 },
		});
	};

	keyboardDidHide = e => {
		// Animation types easeInEaseOut/linear/spring
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		this.setState({
			visibleHeight: Metrics.screenHeight,
			topLogo: { width: Metrics.screenWidth - 80, height: Metrics.screenHeight - 500 },
		});
	};

	handlePressLogin = () => {
		const { username, password } = this.state
		this.props.loginVerification({
			variables: {
			  username,
			  password
			}
		}).then( res => { 
			console.log(client)
			client.resetStore()
			if (this.state.remember) {
				this.props.attemptLogin(res);
			}
			else {
				this.props.attemptTransientLogin(res);
			}
		})
		.catch( res =>{
				const errors = res.graphQLErrors.map( error => error.message);
				Toast.show({
					text: errors,
					position: 'top',
					buttonText: 'Okay',
					duration: 2000
				})
			}
		)
		this.isAttempting = true
	};

	handleChangeUsername = text => {
		this.setState({ username: text });
	};

	handleChangePassword = text => {
		this.setState({ password: text });
	};

	handleRememberPress = () => {
		this.setState({remember: !this.state.remember});
	}

	render() {
		const { username, password } = this.state;
		const { fetching } = this.props;
		const editable = !fetching;
		const textInputStyle = editable ? Styles.textInput : Styles.textInputReadonly;
		return (
			<Container>
				<Content padder style={Styles.container} extraScrollHeight={55}>
				  <View style={Styles.logoContainer}>
					<Image source={Images.alternateLogoWhite} style={Styles.logo} />
					</View>
					<Form>
						<Item regular style={Styles.input}>
							<Icon active name='md-mail' style={{color:'white'}}/>
							<Label style={Styles.labelText} >Email</Label>
							<Input
							  style={{color:'white'}}
								ref="username"
								value={username}
								editable={editable}
								keyboardType="email-address"
								returnKeyType="next"
								autoCapitalize="none"
								autoCorrect={false}
								onChangeText={this.handleChangeUsername}
								underlineColorAndroid="transparent"
								onSubmitEditing={() => this.password._root.focus()}
							/>
						</Item>
						<Item regular style={Styles.input}>
							<Icon active name='md-lock' style={{color:'white'}}/>
							<Label style={Styles.labelText}>Password</Label>
							<Input
							  style={{color:'white'}}
								ref={ref => (this.password = ref)}
								value={password}
								editable={editable}
								keyboardType="default"
								returnKeyType="go"
								autoCapitalize="none"
								autoCorrect={false}
								secureTextEntry
								onChangeText={this.handleChangePassword}
								underlineColorAndroid="transparent"
								onSubmitEditing={this.handlePressLogin}
							/>
						</Item>
						<Item style={{borderBottomWidth: 0, alignItems: 'center' , flexDirection: 'row-reverse' }}>
						    <Left>
								  <CheckBox color='limegreen' checked={this.state.remember} onPress={this.handleRememberPress}/>
								</Left>
								<Title style={{ color: 'white', flex: -1}}> Remember Credentials </Title>
						</Item>
					</Form>
		      { this.props.fetching && <Spinner/> }
				</Content>
				{(Platform.OS === 'ios') ? <KeyboardAvoidingView behavior='padding'>
        <Footer>
          <FooterTab>
            <Button active danger full onPress={this.handlePressBack}>
              <Text style={{ color: 'white' }} >CANCEL</Text>
            </Button>
          </FooterTab>
					<FooterTab>
						<Button active success full onPress={this.handlePressLogin}>
						  <Text style={{color: 'white'}}>LOG IN</Text>
						</Button>
					</FooterTab>
        </Footer>
				</KeyboardAvoidingView>
				:
        <Footer>
          <FooterTab>
            <Button active danger full onPress={this.handlePressBack}>
              <Text style={{ color: 'white' }} >CANCEL</Text>
            </Button>
          </FooterTab>
					<FooterTab>
						<Button active success full onPress={this.handlePressLogin}>
						  <Text style={{color: 'white'}}>LOG IN</Text>
						</Button>
					</FooterTab>
		</Footer> }
			</Container>
			
		);
	}
}

const mapStateToProps = state => {
	return {
		fetching: state.login.fetching,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		attemptLogin: (token) => dispatch(LoginActions.loginRequest(token)),
		attemptTransientLogin: (token) => dispatch(TransientLoginActions.transientLoginRequest(token)),
	};
};

const UserLoginQuery = gql`
mutation UserLogin($username: String!,$password: String!) {
  login(email: $username, password: $password) {
    token
  }
}
`

export default compose(
	graphql(UserLoginQuery, { name: 'loginVerification' }),
	connect(mapStateToProps, mapDispatchToProps))(LoginScreen);
