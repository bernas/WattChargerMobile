import React, { Component, PropTypes} from 'react'
import { BackHandler, Image } from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid";
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, List, ListItem, Card, CardItem, Picker, Item, Form, Label, Input, View} from 'native-base'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { connect } from 'react-redux'
import { Images,Colors} from "../Themes";
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import SliderEntry from '../Components/SliderEntry'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AddEvScreenStyle'
import { sliderWidth, itemWidth } from '../Components/Styles/SliderEntryStyle';

class AddEvScreen extends Component {

  static propTypes = {
		dispatch: PropTypes.func,
		fetching: PropTypes.bool,
		attemptLogin: PropTypes.func,
	};


  constructor (props) {
     super(props)
     this.state = {
      slider1ActiveSlide: 0,
      name: '',
      brand: null,
      model: null,
      year:null,
      error: false,
      evs: [] 
    };
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePressBack = () => {
    this.props.navigation.goBack();
  };

  _renderItemWithParallax ({item, index}, parallaxProps) {
    return (
        <SliderEntry
          data={item}
          even={(index + 1) % 2 === 0}
          parallax={true}
          parallaxProps={parallaxProps}
        />
    );
  }

  filtercar = (ev) => {
    return ev.brand === this.state.brand
  }


  removerepeatmodel = () => {
   const array = this.props.getEvs.listCars.filter(this.filtercar).map((ev) => ev.model)
   const nodups = Array.from(new Set(array));
   const retval = nodups.map((ev) => this.createoption(ev))
   return retval

 }

 removerepeatbrand = () => {

   const array = this.props.getEvs.listCars.map((ev) => ev.brand)
   const nodups = Array.from(new Set(array));
   const retval = nodups.map((ev) => this.createoption(ev))
   return retval

 }

 removerepeatyear = () => {

   const array = this.props.getEvs.listCars.filter(this.filtercar).map((ev) => ev.year.toString())
   const nodups = Array.from(new Set(array));
   const retval = nodups.map((ev) => this.createoption(ev))
   return retval

 }

createoption = (ev) =>{
  return (<Item label={ev} value={ev} key={ev}/>)
}

checkConstraint = (ev) => {
  let retval = false
    if(this.state.brand){
        if(ev.brand == this.state.brand){retval = true}
        else{ return false}
    }
    if(this.state.model){
      if(ev.model == this.state.model){retval = true}
      else{return false}
    }
    if(this.state.year){
      if(ev.year == this.state.year){retval = true}
      else{return false}
    }
    return retval
}


//{`${ev}`}

  onValueChange(value: string) {
        this.setState({
        brand: value,
        model: null,
        year: null,
        ev: null
      });
    }

  onValueChange2(value: string) {
    this.setState({
      model: value,
      ev: null,
    });
  }

  onValueChange3(value: string) {
    this.setState({
      year: value,
      ev: null
    });
  }

  handleChangeRange = text => {
		this.setState({ range: text });
	};

	handleChangePending = text => {
		this.setState({ pending: text });
  };

  handleChangeName = text => {
    this.setState({error: false})
    this.setState({ name: text})
  }
  
  handleSelectedEV = (ev) => {
    this.setState({brand: ev.brand, model: ev.model, year: ev.year.toString(), ev: ev})
  }
  
  handleAddEv = async () => {
    const id = this.state.ev.id
    const savings = 0.0
    const consumption = 0.0
    if (this.state.name=="") {
      this.setState({error: true})
      alert("You need to set a name to your EV!")
    }
    else {
      const name = this.state.name
      try{
        const result = await this.props.addEv({
          variables: {
            id,
            savings,
            consumption,
            name
          }
        })
        this.props.navigation.state.params.handlerFunction()
        this.handlePressBack()
      }
      catch(error){
      }
    }
  }


  createCars = () => {
    return this.props.getEvs.listCars.filter(this.checkConstraint).map((ev) => this.createMap(ev))
 }

 createMap = (ev) => {
   return( 
     {
       ev: ev,
       title: ev.brand,
       subtitle: ev.model,
       illustration:  ev.avatar,
       callbackFunc: this.handleSelectedEV,
   }
   )
}


  render () {
    if (this.props.getEvs && this.props.getEvs.loading){return <Text>loading</Text>}
   if (this.props.getEvs && this.props.getEvs.error){return <Text>error</Text>}
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this.handlePressBack}>
              <Icon style={styles.icon} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerTitle}>ADD EV</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container}>
            <Form style={styles.list}>
              {!this.state.error && <Item regular style={{ marginBottom: 20}}>
                <Icon active name='md-car' />
							  <Input
                  ref="name"
                  placeholder="Name"
                  value={this.state.name}
							  	editable={true}
							  	keyboardType="default"
							  	returnKeyType="next"
							  	autoCorrect={false}
							  	onChangeText={this.handleChangeName}
							  	underlineColorAndroid="transparent"
							  />
              </Item>}
              {this.state.error && <Item error regular style={{ marginBottom: 20}}>
                <Icon active name='md-car' />
							  <Input
                  ref="name"
                  placeholder="Name"
                  value={this.state.name}
							  	editable={true}
							  	keyboardType="default"
							  	returnKeyType="next"
							  	autoCorrect={false}
							  	onChangeText={this.handleChangeName}
							  	underlineColorAndroid="transparent"
							  />
              </Item>}
              <Item regular style={{ marginBottom: 20 }}>
                  <Picker
                    iosHeader={ <Title style={{color: 'white'}}>Brand</Title> }
                    placeholder="Select a Brand"
                    mode="dialog"
                    headerStyle={{ backgroundColor: Colors.background }}
                    headerBackButtonTextStyle={{ color:Colors.text}}
                    selectedValue={this.state.brand}
                    onValueChange={this.onValueChange.bind(this)}
                    >
                  {/*  <Item label="Nissan" value="key0" />
                    <Item label="Hyundai" value="key1" />
                    <Item label="Tesla" value="key2" /> */}
                    {this.removerepeatbrand()}
                  </Picker>
              </Item>
              <Item regular style={{ marginBottom: 20 }}>
                  <Picker
                    iosHeader={ <Title style={{color: 'white'}}>Model</Title> }
                    placeholder="Select a Model"
                    mode="dropdown"
                    headerStyle={{ backgroundColor: Colors.background }}
                    headerBackButtonTextStyle={{ color:Colors.text}}
                    selectedValue={this.state.model}
                    onValueChange={this.onValueChange2.bind(this)}
                    >
                  {/*Object.keys(this.state.models).map((key) => {
                      return (<Item label={this.state.models[key]} value={key} key={key}/>)
                  })*/}
                      {this.removerepeatmodel()}
                  </Picker>
              </Item>
              <Item regular style={{ marginBottom: 20}}>
                  <Picker
                    iosHeader={ <Title style={{color: 'white'}}>Year</Title> }
                    placeholder="Select a Year"
                    mode="dropdown"
                    headerStyle={{ backgroundColor: Colors.background }}
                    headerBackButtonTextStyle={{ color:Colors.text}}
                    selectedValue={this.state.year}
                    onValueChange={this.onValueChange3.bind(this)}
                    >
                    {this.removerepeatyear()}
                  </Picker>
              </Item>
            </Form>
            <View>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={this.createCars()}
                  renderItem={this._renderItemWithParallax}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={0}
                  inactiveSlideScale={0.94}
                  inactiveSlideOpacity={0.7}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  loop={false}
                  loopClonesPerSide={2}
                  autoplay={false}
                  autoplayDelay={500}
                  autoplayInterval={3000}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                <Pagination
                  dotsLength={this.createCars().length}
                  activeDotIndex={this.state.slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor='black'
                  dotStyle={styles.paginationDot}
                  inactiveDotColor='black'
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
            </View>
        </Content>
        { (this.state.ev!=null) && <Footer>
          <FooterTab>
            <Button active info full onPress={this.handleAddEv}>
              <Text style={{ color: 'white' }} >SUBMIT</Text>
            </Button>
          </FooterTab>
        </Footer> }
      </Container>
    )
  }
}

const ADD_EV_MUTATION = gql`
mutation addEvToUser($id:ID!,$savings: Float,$consumption: Float, $name:String) {
  addEvToUser(id:$id,
  stats:{savings:$savings,consumption:$consumption, name:$name}){
    savings,
    consumption,
    name
  }
}
`

export const GET_EVS_QUERY = gql`
query UserEvsQuery{listCars{
      id,
      avatar,
      brand,
      model,
      chargingStandard,
      year
    }
  }
`

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default compose(
  graphql(GET_EVS_QUERY, {name:'getEvs'}),
  graphql(ADD_EV_MUTATION, {name:'addEv'}),
  connect(mapStateToProps, mapDispatchToProps)) (AddEvScreen)
