import React, { Component } from 'react'
import { BackHandler, Image} from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, Card, CardItem, Thumbnail, Item, Input, Toast } from 'native-base'
import { connect } from 'react-redux'
import { Images} from "../Themes";
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import {PropTypes} from 'prop-types';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/SocialScreenStyle'

class SocialScreen extends Component {

  constructor(props){
    super(props);

    this.state = { 
          "feeds": null
    };
  }

  timeDifference(current, previous) {

    const milliSecondsPerMinute = 60 * 1000
    const milliSecondsPerHour = milliSecondsPerMinute * 60
    const milliSecondsPerDay = milliSecondsPerHour * 24
    const milliSecondsPerMonth = milliSecondsPerDay * 30
    const milliSecondsPerYear = milliSecondsPerDay * 365
  
    const elapsed = current - previous
  
    if (elapsed < milliSecondsPerMinute / 3) {
      return 'just now'
    }
  
    if (elapsed < milliSecondsPerMinute) {
      return 'less than 1 min ago'
    }
  
    else if (elapsed < milliSecondsPerHour) {
      return `${Math.round(elapsed/milliSecondsPerMinute)  }min ago`
    }
  
    else if (elapsed < milliSecondsPerDay ) {
      return `${Math.round(elapsed/milliSecondsPerHour )  }h ago`
    }
  
    else if (elapsed < milliSecondsPerMonth) {
      return `${Math.round(elapsed/milliSecondsPerDay)  } days ago`
    }
  
    else if (elapsed < milliSecondsPerYear) {
      return `${Math.round(elapsed/milliSecondsPerMonth)  } mo ago`
    }
  
    return `${Math.round(elapsed/milliSecondsPerYear )  } years ago`
  }

  timeDifferenceForDate(date) {
    const now = new Date().getTime()
    const updated = new Date(date).getTime()
    return this.timeDifference(now, updated)
  }

  getPostsToRender() {
    return this.props.allPostsQuery.allPosts;
  }

  renderPosts(item, index) {
    return (
      <Card padder style={{ backgroundColor: "grey" }} key={item.id}>
            <CardItem>
              <Left style={{ width: "160%" }}>
                <Thumbnail source={{ uri: `${item.user.avatar}` }}  style={{ width: 40, height: 40 }} />
                <Body>
                  <Text>{item.user.username}</Text>
                  <Text note style={{ fontSize: 12 }}>{`since ${new Date(item.user.insertedAt).getFullYear()}`}</Text>
                </Body>
              </Left>
              <Right>
                <Text note>{this.timeDifferenceForDate(item.insertedAt)}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Text style={{ fontWeight: "bold" }}>{item.title}</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{ fontSize: 14 }}>
                  {item.body}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-up" />
                  <Text style={{color: 'grey'}}> {item.upvotes}</Text>
                </Button>
                <Button transparent>
                  <Icon style={{color: 'grey'}} name="md-arrow-round-down" />
                  <Text style={{color: 'grey'}}> {item.downvotes}</Text>
                </Button>
              </Left>
              <Right>
                <Button transparent onPress={this.handlePostComments.bind(this, item)}>
                  <Icon style={{color: 'grey'}} name="chatbubbles" />
                  <Text style={{color: 'grey'}}> {item.comments.length} comments</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
    )
  };

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }

  handlePostComments = ( post ) => {
    this.props.navigation.navigate("PostScreen", { post: post });
  };

  render () {
    if (this.props.allPostsQuery && this.props.allPostsQuery.loading) {
      return (
        <Container>
          <Header style={ styles.header }>
            <Left/>
            <Body>
              <Title style={ styles.title }>Social</Title>
            </Body>
            <Right />
          </Header>
          <Content padder style={ styles.container }>
            <Text>Loading the posts for you</Text>
          </Content>
        </Container>
      );
    }
    if (this.props.allPostsQuery && this.props.allPostsQuery.error) {
      return (
        <Container>
          <Header style={ styles.header }>
            <Left/>
            <Body>
              <Title style={ styles.title }>Social</Title>
            </Body>
            <Right />
          </Header>
          <Content padder style={ styles.container }>
            <Text>An error has occurred</Text>            
          </Content>
        </Container>
      );
    }

    const postsToRender = this.getPostsToRender();

    const feedPostsList = postsToRender.map((result, index) => {
      return this.renderPosts(result, index);
    });

    return (
      <Container>
        <Header style={ styles.header }>
          <Left/>
          <Body>
            <Title style={ styles.title }>Social</Title>
          </Body>
          <Right />

        </Header>
        <Header>
          <Left>
            <Item rounded style={{ width: "170%" }}>
              <Icon name="md-search" />
              <Input style={{ height:20 }} placeholder="Search for a post" />
            </Item>
          </Left>
          <Right>
            <Button transparent style={{ height: 30 }} onPress={() => null}>
              <Icon name="md-menu"/>
            </Button>
          </Right>
        </Header>

        <Content padder style={ styles.container }>
          { feedPostsList }
        </Content>
      </Container>
    )
  }
}

SocialScreen.propTypes = {
  allPostsQuery: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    error: PropTypes.object,
    allPosts: PropTypes.array
  }).isRequired
};

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const AllPostsQuery = gql`
query AllPostsQuery {allPosts{
  id,
  title,
  body,
  downvotes,
  upvotes,
  insertedAt,
  user {
    username,
    avatar,
    insertedAt
  },
  medias {
    media
  },
  comments{
    comment,
    upvotes,
    downvotes,
    insertedAt,
    user{
      username,
      avatar
    },
    medias {
      media
    }
  }
 }
}
`

export default compose(
  graphql(AllPostsQuery, { name: 'allPostsQuery' }),  
  connect(mapStateToProps, mapDispatchToProps))(SocialScreen)
