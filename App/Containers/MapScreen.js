import React, { PropTypes, Component } from 'react'
import { Images, Colors } from '../Themes'
import { View, BackHandler } from 'react-native'
import { InputGroup, Segment, Fab, Item, Input, Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab, Thumbnail } from 'native-base'
import { connect } from 'react-redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import ChargerMap from '../Components/ChargerMap.js'
import SearchResults from '../Components/SearchResults.js'
import ChargerDetails from '../Components/ChargerDetails.js'
import CarPicker from '../Components/CarPicker.js'

import MapActions from '../Redux/MapRedux'
import ChargerMapActions from '../Redux/ChargerMapRedux'

// Styles
import styles from './Styles/MapScreenStyle'

class MapScreen extends Component {
  static propTypes = {

  }

  constructor() {
      super();
      this.state = {
        searchText: "",
        active: false,
        showMarkerInfo: false,
        selectedChargerID: null,
        predictions: {},
        locations: {},
        cars: [],
        carPicker: false,
        chosenCar: {},
        topLeftImage: Images.testcar,
      };
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    })
  }
  
  componentWillMount () {
    this.props.getChargers()
    this.setState({locations: this.props.locations})
  }

  componentWillReceiveProps (nextProps) {
    if(nextProps.locations!=null) {
      this.setState({locations: nextProps.locations})
    }
    if(nextProps.predictions!=null) {
      this.setState({predictions: nextProps.predictions})
    }
    if(nextProps.getUserEvs.getEvsFromUser.length > 0) {
      this.setState({
        chosenCar: nextProps.getUserEvs.getEvsFromUser[0],
         topLeftImage: {uri: nextProps.getUserEvs.getEvsFromUser[0].ev.avatar},
      })
    }
  }

	handlePressRouting = () => {
		this.props.navigation.navigate("RoutePlannerScreen");
  };

	handlePressSettings = () => {
		this.props.navigation.navigate("SettingsScreen");
  };

  handlePressSocial = () => {
    this.props.navigation.navigate("SocialScreen");
  };

  handlePressChargerComments = () => {
    this.setState({showMarkerInfo: false});
    this.props.navigation.navigate("ChargerCommentsScreen");
  };

  handlePressMarker = (data) => {
    console.log(data)
    this.setState({selectedChargerID: data._id,showMarkerInfo: true})
  };

  handleHideChargerDetails = () => {
    this.setState({ showMarkerInfo: false})
  }

  handlePressCarPicker = () => {
    this.props.getUserEvs.refetch()
    this.setState({cars: this.props.getUserEvs.getEvsFromUser})
    this.setState({carPicker: true})
  }

  handleChosenCar = (ev) => {
    this.setState({chosenCar: ev, topLeftImage: {uri: ev.ev.avatar }, carPicker: false})
  }

  handleSearch = (inputText) => {
    this.setState({ searchText: inputText })

    if (inputText=="") {
      this.setState({predictions: {}})
    }
    else {
      this.props.attemptSearch(inputText);
      if(this.props.predictions!=null) {
        this.setState({predictions: this.props.predictions})
      }
    }
  }

  render () {
    const { searchText } = this.state
    return (
      <Container scrollable={false}>
        <Header searchBar style={styles.header} >
          <Left style={styles.leftHeaderContainer}>
            <Button transparent onPress={this.handlePressCarPicker} > 
              <Thumbnail square style={{resizeMode: 'contain'}} source={this.state.topLeftImage} style={styles.topLeftButton}/>
            </Button>
          </Left>
          <Body scrollable={false} style={styles.bodyHeaderContainer}>
            <Item style={styles.searchBar}>
              <Icon name="md-search" style={{color:'white'}} />
              <Input placeholder="Find Charger..." 
              placeholderTextColor={Colors.ricePaper}
              style={{color: 'white'}}
              ref={ref => (this._searchBar) = ref}
              editable={!this.state.carPicker}
              value={searchText}
						  autoCapitalize="none"
						  autoCorrect={false}
              onChangeText={(text) => this.handleSearch(text)}
              onFocus={() => {
                this.setState({ active: true });
                this.setState({showMarkerInfo: false});
              }}
            />
            </Item>
          </Body>
          <Right style={styles.rightHeaderContainer}>
            { (!this.state.active && !this.state.carPicker) && <Button transparent style={styles.topRightButton} onPress={() => null}>
              <Icon name="md-menu" style={{color: 'white'}}/>
            </Button>}
            { (this.state.active || this.state.carPicker) && <Button transparent style={styles.topRightButton} onPress={() => {
              this.setState({active: false, carPicker: false});
              this.setState({ searchText: ""});
              this._searchBar._root.blur();
            }}>
              <Icon name="md-close" style={{color: 'white'}}/>
            </Button>}
          </Right>
        </Header>
        <Content>
          <Container><ChargerMap handleHidePressMarker={this.handleHidePressMarker} handlePressMarker={this.handlePressMarker} locations={this.state.locations}/></Container>
        </Content>
        <Fab
          style={{flex: 1}}
          active={this.state.active}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#34A34F' }}
          position="bottomRight"
          onPress={this.handlePressRouting}>
          <Icon name="md-car" />
        </Fab>
        { (this.state.active) &&
        <SearchResults style={styles.searchResultsWrapper} predictions={{pred: this.state.predictions, inText: this.state.searchText}}/>
        }
        <ChargerDetails visible={this.state.showMarkerInfo} close={this.handleHideChargerDetails} id={this.state.selectedChargerID} comments={this.handlePressChargerComments}/> 
        { (this.state.carPicker) && 
        <CarPicker style={styles.searchResultsWrapper} cars={this.state.cars} callback={this.handleChosenCar}/>
        }
      </Container>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    fetching: state.map.fetching,
    predictions: state.map.predictions,
    locations: state.chargermap.locations
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    attemptSearch: (inputText) => dispatch(MapActions.searchRequest(inputText)),
    getChargers: () => dispatch(ChargerMapActions.chargerMapRequest({}))
  }
}

const USER_EVS_QUERY = gql`
  query UserEvsQuery {
    getEvsFromUser {
      ev {
        avatar,
        brand,
        chargingStandard,
        maxCharge,
        model,
        year,
        weight,
        power,
        maxSpeed,
        aerodynamicCoeficient,
        frictionCoefficient,
        battery,
        decharge,
        charge,
        moteur,
        precup,
        category
      },
      id,
      consumption,
      savings,
      name
    }
  }
`

export default compose(
  graphql(USER_EVS_QUERY, { name: 'getUserEvs' }),
  connect(mapStateToProps, mapDispatchToProps)
)(MapScreen)